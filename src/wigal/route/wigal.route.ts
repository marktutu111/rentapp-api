import * as joi from "joi";
import Handler from "../handler/wigal.handler";

const route=[
    {
        path:'/debit',
        method:'POST',
        config:{
            validate:{
                payload:joi.object(
                    {
                        "amount":joi.string(),
                        "clienttransid":joi.string(),
                        "nickname":joi.string(),
                        "paymentoption":joi.string(),
                        "walletnumber":joi.string()
                    }
                )
            }
        },
        handler:Handler.pay
    }
]


export default route;