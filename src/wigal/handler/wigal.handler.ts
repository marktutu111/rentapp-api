

const pay=async(request,h)=>{
    try {
        const data=request.payload;
        const response=await request.server.methods.WigalSendPayment(
            {
                type:'DEBIT',
                payload:data
            }
        );
        return h.response(
            {
                success:true,
                data:response
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    pay
}