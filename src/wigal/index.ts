import sendRequest from "./process/wigal.process";
import route from "./route/wigal.route";

const wigal = {
    pkg: {
        name: 'wigal-api',
        version: '1'
    },
    register: async (server) => {
        server.route(route);
        server.method(
            'WigalSendPayment',
            sendRequest
        );

    }
}


export default wigal;