import * as request from "request";
import * as path from "path";
const config=require(path.resolve('src/config.json'));


const sendRequest=(
    {
        type,
        payload
    }
) => new Promise((resolve,reject)=>{
    const {apikey,appid}=config['wigal'];
    let url: string = "";
    let method=null;
    let headers=null;
    switch (type) {
        case 'DEBIT':
            url="https://api.reddeonline.com/v1/receive";
            method='post';
            headers={
                'Content-type':'application/json;charset=UTF-8',
                'apikey':apikey,
            }
            break;
        case 'CREDIT':
            url="https://api.reddeonline.com/v1/cashout";
            method='post';
            headers={
                'Content-type':'application/json;charset=UTF-8',
                'apikey':apikey
            }
            break;
        default:
            url=`https://api.reddeonline.com/v1/status/${payload}`;
            method='get';
            headers={
                'Content-type':'application/json;charset=UTF-8',
                'apikey':apikey,
                'appid':appid
            }
            break;
    }

    const data=Object.assign(payload, { appid:appid, nickname:'16th August 85, Villa' });
    const options={
        url: url,
        headers:headers,
        body: JSON.stringify(data)
    }

    request[method](options, (err,res,bd) => {
        try {
            if (err) reject(err);
            resolve(JSON.parse(bd));
        } catch (err) {
            reject(err);    
        }
    });
    
})


export default sendRequest;