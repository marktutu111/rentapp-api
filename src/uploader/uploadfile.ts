import * as fs from "fs";
import * as path from "path";

const uploader = ({data,type,id})=>new Promise((resolve, reject) => {
    const _path=path.resolve(`uploads/${id}.${type}`);
    var base64Data = data.split(',')[1];
    fs.writeFile(_path,base64Data,'base64',(err)=>{
        // if (err)return reject(err);
        resolve(_path);
    })
});

const downloader = (path:string)=>new Promise((resolve, reject) => {
    fs.readFile(path,(err,data)=>{
        if (err)return reject(err);
        resolve(data);
    })
});

const deleteFile = (path:string)=>new Promise((resolve, reject) => {
    fs.unlink(path,(err)=>{
        if (err)return reject(false);
        resolve(true);
    })
});


export { uploader,downloader,deleteFile }