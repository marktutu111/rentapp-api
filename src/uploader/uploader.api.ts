import { uploader,downloader,deleteFile } from "./uploadfile";

const api={
    pkg:{
        name:'uploader',
        version:'1',
    },
    register:async(server)=>{
        server.method('Uploader',uploader);
        server.method('Downloader',downloader);
        server.method('DeleteFile',deleteFile);
    }
}


export default api;