import * as request from "request";
import { IArkeselSmsPayload } from "./arkesel.model";
import { formatePhonenumber } from "../app/utils/formate-phone-number";

const sendsms = ({ message, destination }: IArkeselSmsPayload): Promise<any> =>
  new Promise((resolve, reject) => {
    const url: string = `https://sms.arkesel.com/api/v2/sms/send`;
    const recipients = Array.isArray(destination)
      ? destination.map((d) => formatePhonenumber(d))
      : [formatePhonenumber(destination)];
    const body = JSON.stringify({ sender: "16Aug85vila", message, recipients });
    const headers = {
      "Content-type": "application/json",
      "api-key": "Onk1ZG5tRlc4cFFOM3g4NWY=",
    };
    request.post(url, { headers, body }, (err, res, bd) => {
      try {
        if (err) throw Error(err);
        resolve(JSON.parse(bd));
      } catch (err) {
        reject(err);
      }
    });
  });

export default sendsms;
