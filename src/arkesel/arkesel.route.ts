import { ServerRoute } from "hapi";
import * as joi from "joi";
import Handler from "./arkesel.handler";

const route: ServerRoute[] = [
  {
    path: "/sms/send",
    method: "POST",
    options: {
      auth: false,
      validate: {
        payload: joi.object({
          message: joi.string().required(),
          destination: joi.string().required(),
        }),
      },
    },
    handler: Handler.smsHandler,
  },
];

export default route;
