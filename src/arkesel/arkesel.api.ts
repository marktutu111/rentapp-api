import { Server } from "hapi";
import Arkesel from "./arkesel.process";
import route from "./arkesel.route";

const api = {
  name: "arkesel",
  version: "1",
  register: async (server: Server) => {
    server.method("sendsmsArkesel", Arkesel);
    server.route(route);
  },
};

export default api;
