import { Request, ResponseToolkit } from "@hapi/hapi";
import { IArkeselSmsPayload } from "./arkesel.model";
import Arkesel from "./arkesel.process";

const smsHandler = async (request: Request, h: ResponseToolkit) => {
  try {
    const data = request.payload as IArkeselSmsPayload;
    const response = await Arkesel(data);
    if (response.status !== "success") {
      throw Error(response.message);
    }
    return h.response({
      success: true,
      message: "success",
      data: response,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

export default {
  smsHandler,
};
