import * as mongoose from "mongoose";

const schema = new mongoose.Schema({
  name: String,
  id: String,
  type: String,
  data: String,
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model("storage", schema);
