

const createReport=async(request,h)=>{
    try {
        const data=request.payload;
        const results=await request.server.methods.dbReports.create(
            data
        );
        if(!results || !results._id){
            throw Error(
                'Report not sent'
            )
        }
        return h.response(
            {
                success:true,
                message:'Your report has been submitted successfully',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



const getReports=async(request,h)=>{
    try {
        const results=await request.server.methods.dbReports.find(
            {}
        ).populate('tenant').populate('rentId').populate('room').sort(
            {
                createdAt:-1
            }
        )
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getReportByTenants=async(request,h)=>{
    try {
        const {id}=request.params;
        const results=await request.server.methods.dbReports.find(
            {
                tenant:id
            }
        ).populate('tenant').populate('rentId').populate('room').sort(
            {
                createdAt:-1
            }
        )
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


export default {
    createReport,
    getReportByTenants,
    getReports
}