import * as joi from "joi";
import Handler from "../handler/reports.handler";
import { ServerRoute } from "@hapi/hapi";

const routes: ServerRoute[] = [
  {
    path: "/new",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          room: joi.string().optional(),
          tenant: joi.string().optional(),
          rentId: joi.string().optional(),
          sender: joi
            .string()
            .required()
            .valid("TENANT")
            .valid("LANDLORD")
            .default("TENANT"),
          title: joi.string().required(),
          message: joi.string().required(),
        }),
      },
    },
    handler: Handler.createReport,
  },
  {
    path: "/get",
    method: "GET",
    handler: Handler.getReports,
  },
  {
    path: "/get/{id}",
    method: "GET",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.getReportByTenants,
  },
];

export default routes;
