import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        tenant:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'tenants',
            default:null
        },
        rentId:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'rents',
            default:null
        },
        room:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'rooms',
            default:null
        },
        sender:{
            type:String,
            default:'TENANT',
            enum:[
                'TENANT',
                'LANDLORD'
            ]
        },
        title:{
            type:String,
            default:null
        },
        message:{
            type:String,
            default:null
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)


export default mongoose.model(
    'reports',
    schema
)