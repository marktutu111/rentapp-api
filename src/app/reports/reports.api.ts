import routes from "./routes/reports.route";
import db from "./db/reports.db";

const api={
    pkg:{
        name:'reports',
        version:'1'
    },
    register:async(server)=>{
        server.method(
            'dbReports',
            db
        );
        server.route(routes);
    }
}

export default api;