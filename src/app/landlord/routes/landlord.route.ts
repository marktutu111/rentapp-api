import * as joi from "joi";
import Handler from "../handler/landlord.handler";
import { ServerRoute } from "@hapi/hapi";

const routes: ServerRoute[] = [
  {
    path: "/new",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          name: joi.string().required(),
          phone: joi.string().required(),
          password: joi.string().required(),
        }),
      },
    },
    handler: Handler.addNew,
  },
  {
    path: "/login",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          phone: joi.string().required(),
          password: joi.string().required(),
        }),
      },
    },
    handler: Handler.login,
  },
  {
    path: "/update",
    method: ["PUT", "POST"],
    options: {
      validate: {
        payload: joi.object({
          id: joi.string().required(),
          data: joi.object(),
        }),
      },
    },
    handler: Handler.update,
  },
  {
    path: "/get",
    method: "GET",
    handler: Handler.getall,
  },
];

export default routes;
