import * as mongoose from "mongoose";

const schema = new mongoose.Schema({
  name: {
    type: String,
    default: null,
  },
  phone: {
    type: String,
    default: null,
  },
  blocked: {
    type: Boolean,
    default: false,
  },
  password: {
    type: String,
    default: null,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model("landlord", schema);
