import db from "./db/landlord.db";
import routes from "./routes/landlord.route";


const api={
    pkg:{
        name:'landlord',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbLandlords',db);
        server.route(routes);
    }
}

export default  api;