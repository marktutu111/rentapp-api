

const addNew=async(request,h)=>{
    try {
        const data=request.payload;
        const result=await request.server.methods.dbLandlords.create(
            data
        );
        if(!result || !result._id){
            throw Error(
                'Something went wrong, request not completed'
            )
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const update=async(request,h)=>{
    try {
        const {
            id,
            data
        }=request.payload;
        let filter={};
        Object.keys(data).forEach(key=>{
            if(key && data[key]){
                filter[key]=data[key];
            }
        })
        const response=await request.server.methods.dbLandlords.findOneAndUpdate(
            {
                _id:id
            },
            {
                $set:filter
            },
            {new:true}
        )
        if(!response || !response._id){
            throw Error(
                'Account not updated'
            )
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:response
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getall=async(request,h)=>{
    try {
        const results=await request.server.methods.dbLandlords.find(
            {}
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const login=async(request, h)=>{
    try {

        const { 
            phone, 
            password 
        } = request.payload;
        const user = await request.server.methods.dbLandlords.findOne(
            { 
                phone:phone,
                password:password,
                blocked:false
            }
        );
        if(!user || !user._id){
            throw Error(
                'Account not found'
            )
        }
        return h.response(
            {
                success:true,
                message:'success',
                data:user,
                token:'',
                role:'landlord'
            }
        )
    } catch (err) {
        return h.response(
            {
                success: false,
                message: err.message
            }
        );
    }
}


export default {
    addNew,
    update,
    login,
    getall
}