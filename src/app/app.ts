import Tenants from "./tenants/tenants.api";
import Rents from "./rents/rents.api";
import Documents from "./documents/documents.api";
import Rooms from "./rooms/rooms.api";
import Inspection from "./inspection/inspection.api";
import Payments from "./payments/payments.api";
import Landlords from "./landlord/landlord.api";
import Reports from "./reports/reports.api";
import Utilities from "./utilities/utilities.api";
import Expenses from "./expenses/expenses.api";
import Invoices from "./invoice/invoice.api";
import Settings from "./settings/settings.api";

const api = {
  pkg: {
    name: "app",
    version: "1",
  },
  register: async (server) => {
    server.register([
      {
        plugin: Documents,
        routes: {
          prefix: "/rentcontrol/documents",
        },
      },
      {
        plugin: Settings,
        routes: {
          prefix: "/rentcontrol/settings",
        },
      },
      {
        plugin: Invoices,
        routes: {
          prefix: "/rentcontrol/invoice",
        },
      },
      {
        plugin: Expenses,
        routes: {
          prefix: "/rentcontrol/expenses",
        },
      },
      {
        plugin: Utilities,
        routes: {
          prefix: "/rentcontrol/utilities",
        },
      },
      {
        plugin: Landlords,
        routes: {
          prefix: "/rentcontrol/landlords",
        },
      },
      {
        plugin: Reports,
        routes: {
          prefix: "/rentcontrol/reports",
        },
      },
      {
        plugin: Tenants,
        routes: {
          prefix: "/rentcontrol/tenants",
        },
      },
      {
        plugin: Rents,
        routes: {
          prefix: "/rentcontrol/rents",
        },
      },
      {
        plugin: Rooms,
        routes: {
          prefix: "/rentcontrol/rooms",
        },
      },
      {
        plugin: Inspection,
        routes: {
          prefix: "/rentcontrol/inspection",
        },
      },
      {
        plugin: Payments,
        routes: {
          prefix: "/rentcontrol/payments",
        },
      },
    ]);
  },
};

export default api;
