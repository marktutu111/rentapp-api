import db from "./db/payments.db";
import routes from "./routes/payments.route";
import debitHandler from "./handler/payments.debit";
import callbackHandler from "./handler/callback.handler";

const api={
    pkg:{
        name:'payments',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbPayments',db);
        server.method('processDebit',debitHandler);
        server.method('callbackHandler',callbackHandler);
        server.route(routes);
    }
}

export default api;