import { Request, ResponseToolkit } from "hapi";
import dbPayments from "../db/payments.db";
import * as mongoose from "mongoose";
import * as moment from "moment";
import callback from "./callback.handler";

const filter = async (request: Request, h: ResponseToolkit) => {
  try {
    let query: any = {};
    let { startDate, endDate, tenantId, status, purpose } =
      request.payload as any;
    if (typeof startDate === "string" && typeof endDate === "string") {
      query.createdAt = {
        $gte: moment(startDate).startOf("day").toDate(),
        $lte: moment(endDate).endOf("day").toDate(),
      };
    }
    if (mongoose.Types.ObjectId.isValid(tenantId)) {
      query.tenantId = tenantId;
    }
    if (typeof status === "string") {
      query.status = status;
    }
    if (typeof purpose === "string") {
      query.purpose = purpose;
    }
    const results = await dbPayments
      .find(query)
      .sort({
        createdAt: -1,
      })
      .populate("tenantId", { photo: 0 })
      .populate("rentId")
      .populate("inspectionId");
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getall = async (request: Request, h: ResponseToolkit) => {
  try {
    const results = await dbPayments
      .find({})
      .limit(1000)
      .sort({
        createdAt: -1,
      })
      .populate("tenantId", { photo: 0 })
      .populate("rentId")
      .populate("inspectionId");
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const tenantPayments = async (request, h) => {
  try {
    const { id } = request.params;
    const results = await request.server.methods.dbPayments
      .find({
        tenantId: id,
      })
      .limit(1000)
      .sort({
        createdAt: -1,
      })
      .populate("tenantId", { photo: 0 })
      .populate("rentId")
      .populate("inspectionId")
      .populate("rentId.room");
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const deleteTransaction = async (request, h) => {
  try {
    const { id } = request.params;
    const results = await request.server.methods.dbPayments.findOneAndDelete({
      _id: id,
    });
    if (!results || !results._id) {
      throw Error("Transaction not deleted");
    }
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const summary = async (request: Request | any, h: ResponseToolkit) => {
  try {
    const results = await request.server.methods.dbPayments.aggregate([
      {
        $match: {},
      },
      {
        $facet: {
          transactions: [
            {
              $lookup: {
                from: "tenants",
                localField: "tenantId",
                foreignField: "_id",
                as: "tenantId",
              },
            },
            {
              $lookup: {
                from: "rents",
                localField: "rentId",
                foreignField: "_id",
                as: "rentId",
              },
            },
            {
              $lookup: {
                from: "inspections",
                localField: "inspectionId",
                foreignField: "_id",
                as: "inspectionId",
              },
            },
            {
              $unwind: { path: "$tenantId", preserveNullAndEmptyArrays: true },
            },
            { $unwind: { path: "$rentId", preserveNullAndEmptyArrays: true } },
            {
              $unwind: {
                path: "$inspectionId",
                preserveNullAndEmptyArrays: true,
              },
            },
            { $sort: { createdAt: -1 } },
            { $limit: 10 },
            { $project: { photo: 0 } },
            {
              $group: {
                _id: null,
                items: { $push: "$$ROOT" },
              },
            },
          ],
          today: [
            {
              $match: {
                status: "PAID",
                createdAt: {
                  $gte: moment().startOf("day").toDate(),
                  $lte: moment().endOf("day").toDate(),
                },
              },
            },
            {
              $lookup: {
                from: "tenants",
                localField: "tenantId",
                foreignField: "_id",
                as: "tenantId",
              },
            },
            {
              $lookup: {
                from: "rents",
                localField: "rentId",
                foreignField: "_id",
                as: "rentId",
              },
            },
            {
              $lookup: {
                from: "inspections",
                localField: "inspectionId",
                foreignField: "_id",
                as: "inspectionId",
              },
            },
            {
              $unwind: { path: "$tenantId", preserveNullAndEmptyArrays: true },
            },
            { $unwind: { path: "$rentId", preserveNullAndEmptyArrays: true } },
            {
              $unwind: {
                path: "$inspectionId",
                preserveNullAndEmptyArrays: true,
              },
            },
            {
              $group: {
                _id: null,
                total: { $sum: "$amount" },
                count: { $sum: 1 },
              },
            },
          ],
          week: [
            {
              $match: {
                status: "PAID",
                createdAt: {
                  $gte: moment().startOf("week").toDate(),
                  $lte: moment().endOf("week").toDate(),
                },
              },
            },
            {
              $group: {
                _id: null,
                total: { $sum: "$amount" },
                count: { $sum: 1 },
              },
            },
          ],
          month: [
            {
              $match: {
                status: "PAID",
                createdAt: {
                  $gte: moment().startOf("month").toDate(),
                  $lte: moment().endOf("month").toDate(),
                },
              },
            },
            {
              $group: {
                _id: null,
                total: { $sum: "$amount" },
                count: { $sum: 1 },
              },
            },
          ],
          year: [
            {
              $match: {
                status: "PAID",
                createdAt: {
                  $gte: moment().startOf("year").toDate(),
                  $lte: moment().endOf("year").toDate(),
                },
              },
            },
            {
              $group: {
                _id: null,
                total: { $sum: "$amount" },
                count: { $sum: 1 },
              },
            },
          ],
          paymentType: [
            {
              $match: {
                status: "PAID",
              },
            },
            {
              $group: {
                _id: "$paymentType",
                total: { $sum: "$amount" },
                count: { $sum: 1 },
              },
            },
          ],
          all: [
            {
              $match: {
                status: "PAID",
              },
            },
            {
              $group: {
                _id: null,
                amount: { $sum: "$amount" },
                count: { $sum: 1 },
              },
            },
          ],
        },
      },
    ]);
    return h.response({
      success: true,
      message: "success",
      data: results[0],
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const rentLastPaid = async (request, h) => {
  try {
    const { id } = request.params;
    const results = await request.server.methods.dbPayments
      .findOne({
        rentId: id,
      })
      .sort({ createdAt: 1 })
      .populate("rentId");
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const update = async (request, h: ResponseToolkit) => {
  try {
    const { id, data } = request.payload;
    let filter = {};
    Object.keys(data).forEach((key) => {
      if (key && data[key]) {
        filter[key] = data[key];
      }
    });
    const response = await request.server.methods.dbPayments.findOneAndUpdate(
      {
        _id: id,
      },
      {
        $set: filter,
      },
      { new: true }
    );
    if (!response || !response._id) {
      throw Error("Transaction not updated");
    }
    return h.response({
      success: true,
      message: "Transaction updated successfully",
      data: response,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const callbackHandler = async (request: Request, h: ResponseToolkit) => {
  try {
    const payload = request.payload;
    callback(payload);
    return {
      success: true,
      message: "callback received",
    };
  } catch (err) {
    return {
      success: true,
      message: "callback received",
    };
  }
};

export default {
  getall,
  tenantPayments,
  update,
  rentLastPaid,
  deleteTransaction,
  summary,
  callbackHandler,
  filter,
};
