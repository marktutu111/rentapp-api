import {
  EnumOperators,
  EnumPaymentStatus,
  EPaymentPurpose,
  TPaymentStatus,
} from "../../common/payments.model";
import lazyPay from "../../../lazypay/lazypay.request";

const handler = async (request, transaction) => {
  try {
    const {
      _id,
      amount,
      accountIssuer,
      accountNumber,
      operator,
      tenantId,
      purpose,
      inspectionId,
    } = transaction;

    let response: any = null;
    let status: TPaymentStatus = EnumPaymentStatus.PENDING;
    let message: string = "";
    let accountName: string = "";
    let data: any;
    let transactionRef: string = "";
    let description: string;
    let token: any;

    switch (purpose) {
      case EPaymentPurpose.INSPECTION:
        try {
          const inspection = await request.server.methods.dbInspections.findOne(
            {
              _id: inspectionId,
            }
          );
          accountName = inspection.name;
          description = "Inspection fee";
        } catch (err) {}
        break;
      default:
        try {
          const tenant = await request.server.methods.dbTenants.findOne({
            _id: tenantId,
          });
          accountName = `${tenant.firstname} ${tenant.lastname}`;
          description = "Rent payment";
        } catch (er) {}
        break;
    }

    switch (operator) {
      case EnumOperators.REDDE:
        data = {
          amount: amount,
          clienttransid: _id,
          nickname: accountName,
          paymentoption: accountIssuer,
          walletnumber: accountNumber,
        };
        response = await request.server.methods.WigalSendPayment({
          type: "DEBIT",
          payload: data,
        });
        transactionRef = response["transactionid"];
        switch (response.status) {
          case "OK":
            status = EnumPaymentStatus.PENDING;
            message = `Your request is received, you will receive a prompt on your phone to authorize payment for GHS${transaction["amount"]}.`;
            break;
          default:
            status = EnumPaymentStatus.FAILED;
            message =
              "Sorry we could not process your request, Please contact customer care.";
            break;
        }
        break;
      case EnumOperators.LAZYPAY:
        data = {
          amount: Number(amount),
          account_number: accountNumber,
          account_name: accountName,
          account_issuer: accountIssuer,
          description,
          externalTransactionId: _id,
        };
        token = await lazyPay.getToken();
        response = await lazyPay.debitMomo({
          data,
          token: token["data"],
        });
        transactionRef = response.transactionId;
        switch (response.code) {
          case "01":
            status = EnumPaymentStatus.PENDING;
            message = `Your request is received, you will receive a prompt on your phone to authorize payment for GHS${transaction["amount"]}.`;
            break;
          default:
            status = EnumPaymentStatus.FAILED;
            message = response.message;
            break;
        }
        break;
      case EnumOperators.PPAY:
        data = {
          amount: amount,
          account_number: accountNumber,
          account_name: accountName,
          account_issuer: accountIssuer,
          description: "16th August 85 Villa app payments",
          externalTransactionId: _id,
          callbackUrl:
            "https://webhook.site/9729eddc-1c30-40e9-8187-7b58731a874c",
        };
        token = await request.server.methods.getToken();
        response = await request.server.methods.sendPayment({
          data,
          token: token["data"],
        });
        transactionRef = response.transactionId;
        switch (response.code) {
          case "01":
            status = EnumPaymentStatus.PENDING;
            message = `Your request is received, you will receive a prompt on your phone to authorize payment for GHS${transaction["amount"]}.`;
            break;
          default:
            status = EnumPaymentStatus.FAILED;
            message =
              "Sorry we could not process your request, Please contact support.";
            break;
        }
        break;
      default:
        break;
    }
    await transaction.update({
      transactionRef,
      transaction: response,
      status: status,
    });
    return {
      success: true,
      message: message,
      data: transaction,
    };
  } catch (err) {
    return {
      success: false,
      message: err.message,
      data: null,
    };
  }
};

export default handler;
