import * as mongoose from "mongoose";
import {
  EnumPaymentStatus,
  TPaymentStatus,
  EnumOperators,
  EPaymentPurpose,
} from "../../common/payments.model";
import { formatePhonenumber } from "../../utils/formate-phone-number";
import sendSms from "../../../arkesel/arkesel.process";
import dbPayments from "../db/payments.db";
import dbRents from "../../rents/db/rents.db";
import dbSettings from "../../settings/db/settings.db";
import dbInspections from "../../inspection/db/inspection.db";

const callback = async (payload: any) => {
  try {
    let status: TPaymentStatus = EnumPaymentStatus.PENDING;
    let id: string = payload["clienttransid"] || payload["transactionId"];

    let query = {};
    switch (mongoose.Types.ObjectId.isValid(id)) {
      case true:
        query = { $or: [{ _id: id }, { transactionRef: id }] };
        break;
      default:
        query = { transactionRef: id };
        break;
    }

    const payment = await dbPayments
      .findOneAndUpdate(
        query,
        {
          $set: {
            transaction: payload,
          },
        },
        { new: true }
      )
      .populate("inspectionId")
      .populate("tenantId")
      .populate("rentId")
      .populate("propertyId");

    if (!payment || !payment._id) {
      throw Error("Transaction not found");
    }

    switch (payment.operator) {
      case EnumOperators.REDDE:
        switch (payload.status) {
          case "PROGRESS":
            status = EnumPaymentStatus.PENDING;
            break;
          case "PAID":
            status = EnumPaymentStatus.PAID;
            break;
          default:
            status = EnumPaymentStatus.FAILED;
            break;
        }
        break;
      case EnumOperators.LAZYPAY:
      case EnumOperators.PPAY:
        switch (payload.code) {
          case "01":
            status = EnumPaymentStatus.PENDING;
            break;
          case "00":
            status = EnumPaymentStatus.PAID;
            break;
          default:
            status = EnumPaymentStatus.FAILED;
            break;
        }
        break;
      default:
        throw Error("Operator type not permitted");
    }

    await payment.updateOne({ status });

    const {
      tenantId,
      inspectionId,
      rentId,
      duration,
      durationStartDate,
      durationEndDate,
      propertyId,
      amount,
    } = payment;

    const { receive_sms_alerts }: any = await dbSettings.Settings.findOne({});
    const _rent = await dbRents.findOne({
      _id: rentId,
    });

    let name: string = `${tenantId["firstname"]} ${tenantId["lastname"]}`;
    let phone: string = "";
    let message: string = "";
    let landlordMessage: string = "";

    switch (payment["purpose"]) {
      case EPaymentPurpose.INSPECTION:
        name = inspectionId["name"];
        phone = inspectionId["phone"];
        landlordMessage =
          "Dear Landlord" +
          "\n" +
          name +
          " has made payment to inspect a " +
          payment["property"] +
          ", kindly login to view details" +
          "Thank You";
        await dbInspections.findOneAndUpdate(
          {
            _id: inspectionId["_id"],
          },
          {
            $set: { status },
          }
        );
        break;
      case EPaymentPurpose.RENEWAL:
        try {
          if (status === EnumPaymentStatus.PAID) {
            const { currency, price } = propertyId;
            await dbRents.create({
              room: _rent.room,
              tenant: tenantId,
              duration: duration,
              durationType: _rent.durationType,
              costPerMonth: price,
              paymentType: "MOMO",
              amount: amount,
              rentType: "RENEWAL",
              accomodation: _rent.accomodation,
              status: "PENDING APPROVAL",
              startDate: durationStartDate,
              endDate: durationEndDate,
              currency,
            });
          }
          landlordMessage =
            "Dear Landlord" +
            "\n" +
            name +
            " has made payment to renew their stay in property " +
            payment["property"] +
            ", kindly login to view details" +
            "Thank You.";
        } catch (err) {}
        break;
      case EPaymentPurpose.RENT:
        if (status === EnumPaymentStatus.PAID) {
          await _rent.update({
            status: "PENDING APPROVAL",
          });
        }
        phone = tenantId["phone"];
        landlordMessage =
          "Dear Landlord" +
          "\n" +
          name +
          " has made payment to occupy the property " +
          payment["property"] +
          ", kindly login to view details" +
          "Thank You.";
        break;
      default:
        break;
    }

    if (status === EnumPaymentStatus.PAID) {
      payment
        .update({
          datePaid: Date.now(),
        })
        .catch((err) => null);
      message =
        "Dear " +
        name +
        "," +
        "\n" +
        "Your transaction was processed successfully, " +
        "your landlord will now approve your request." +
        "Thank You";

      if (Array.isArray(receive_sms_alerts)) {
        receive_sms_alerts.forEach((cont: string) => {
          sendSms({
            message:
              "You have pending approvals on 16thAugust85, Villa. " +
              name +
              " has made payment.",
            destination: formatePhonenumber(cont),
          }).catch((err) => null);
        });
      }
    }

    if (status === EnumPaymentStatus.FAILED) {
      message =
        "Dear " +
        name +
        "," +
        "\n" +
        "Your transaction was not successful," +
        "please try again." +
        "\n" +
        "Thank You";
    }

    if (status !== EnumPaymentStatus.PENDING) {
      sendSms({
        message: message,
        destination: formatePhonenumber(phone),
      }).catch((err) => null);
    }
  } catch (err) {
    console.log(err);
  }
};

export default callback;
