import * as joi from "joi";
import Handler from "../handler/payments.handler";
import { ServerRoute } from "@hapi/hapi";
import handler from "../handler/payments.debit";

const routes: ServerRoute[] = [
  {
    path: "/filter",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          tenantId: joi.string().optional().allow(null),
          startDate: joi.string().optional().allow(null),
          endDate: joi.string().optional().allow(null),
          status: joi.string().optional().allow(null),
          purpose: joi.string().optional().allow(null),
        }),
      },
    },
    handler: Handler.filter,
  },
  {
    path: "/get",
    method: "GET",
    handler: Handler.getall,
  },
  {
    path: "/tenant/get/{id}",
    method: "GET",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.tenantPayments,
  },
  {
    path: "/rent/lastpaid/{id}",
    method: "GET",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.rentLastPaid,
  },
  {
    path: "/update",
    method: "PUT",
    options: {
      validate: {
        payload: joi.object({
          id: joi.string().required(),
          data: joi.object(),
        }),
      },
    },
    handler: Handler.update,
  },
  {
    path: "/delete/{id}",
    method: "DELETE",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.deleteTransaction,
  },
  {
    path: "/callback",
    method: "POST",
    handler: Handler.callbackHandler,
  },
  {
    path: "/aggregate",
    method: "GET",
    handler: Handler.summary,
  },
];

export default routes;
