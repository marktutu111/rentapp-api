import * as mongoose from "mongoose";
import {
  EnumOperators,
  EnumPaymentStatus,
  EPaymentPurpose,
} from "../../common/payments.model";

const schema = new mongoose.Schema({
  property: String,
  utilityId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "utilities",
  },
  tenantId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "tenants",
  },
  rentId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "rents",
  },
  invoiceId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "invoices",
  },
  inspectionId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "inspection",
  },
  propertyId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "rooms",
  },
  purpose: {
    type: String,
    uppercase: true,
    default: EPaymentPurpose.INSPECTION,
    enum: EPaymentPurpose,
  },
  description: String,
  accountType: {
    type: String,
    default: null,
    enum: [null, "momo", "card"],
  },
  accountNumber: String,
  accountIssuer: {
    type: String,
    default: null,
    enum: [null, "vodafone", "mtn", "airteltigo"],
  },
  amount: {
    type: Number,
    default: 0,
  },
  datePaid: Date,
  status: {
    type: String,
    uppercase: true,
    default: EnumPaymentStatus.PENDING,
    enum: EnumPaymentStatus,
  },
  paymentType: {
    type: String,
    uppercase: true,
    default: "CASH",
    enum: ["CASH", "MOMO", "BANK DEPOSIT"],
  },
  operator: {
    type: String,
    default: EnumOperators.LAZYPAY,
    enum: EnumOperators,
  },
  period: String,
  duration: String,
  durationStartDate: Date,
  durationEndDate: Date,
  transaction: Object,
  transactionRef: String,
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model("payments", schema);
