import * as joi from "joi";
import Handler from "../handler/inspection.handler";
import { ServerRoute } from "@hapi/hapi";

const routes: ServerRoute[] = [
  {
    path: "/new",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          room: joi.string().required(),
          name: joi.string().required(),
          phone: joi.string().required().length(10),
          date: joi.string().required(),
          time: joi.string().required(),
          accountNumber: joi.string().required(),
          accountIssuer: joi.string().required(),
          amount: joi.alternatives(joi.string(), joi.number()).required(),
          otp: joi.string().required(),
        }),
      },
    },
    handler: Handler.newInspection,
  },
  {
    path: "/get",
    method: "GET",
    handler: Handler.getInspection,
  },
];

export default routes;
