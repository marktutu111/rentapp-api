import * as mongoose from "mongoose";
import { EnumPaymentStatus } from "../../common/payments.model";

const schema=new mongoose.Schema(
    {
        room:{
            type:mongoose.Schema.Types.ObjectId,
            ref:'rooms',
        },
        name:{
            type:String,
            default:null
        },
        phone:{
            type:String,
            default:null
        },
        date:{
            type:Date,
            default:null
        },
        time:{
            type:String,
            default:null
        },
        status:{
            type:String,
            default:'PENDING',
            enum:EnumPaymentStatus
        },
        amount:{
            type:Number,
            default:0
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

export default mongoose.model(
    'inspection',
    schema
)