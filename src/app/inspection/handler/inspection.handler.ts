
const newInspection=async(request,h)=>{
    try {
        const data=request.payload;
        const verified = await request.server.methods.validateOTP(
            {
                phoneNumber:data['phone'],
                otp:data['otp']
            }
        );
        if(!verified){
            throw Error(
                'OTP does not match'
            )
        }
        const room=await request.server.methods.dbRooms.findOne(
            {
                _id:data['room'],
                occupied:false
            }
        );
        if(!room || !room._id){
            throw Error(
                'Room not available for inspection'
            )
        }
        const inspection=await request.server.methods.dbInspections.create(
            data
        );
        if(!inspection || !inspection._id){
            throw Error(
                'Sorry we could process your request'
            )
        };
        const {
            _id
        }=inspection;
        const payment=await request.server.methods.dbPayments.create(
            {
                inspectionId:_id,
                purpose:'INSPECTION',
                accountNumber:data['accountNumber'],
                accountIssuer:data['accountIssuer'],
                amount:data['amount'],
                paymentType:'MOMO'
            }
        );
        const response=await request.server.methods.processDebit(
            request,
            payment
        );
        return h.response(
            response
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getInspection=async(request,h)=>{
    try {
        const results=await request.server.methods.dbInspections.find(
            {}
        ).limit(1000).sort({createdAt:-1}).populate(
            'room'
        )
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}



export default {
    newInspection,
    getInspection
}