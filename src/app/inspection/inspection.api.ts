import db from "./db/inspection.db";
import routes from "./routes/inspection.route";

const api={
    pkg:{
        name:'inspection',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbInspections',db);
        server.route(routes);
    }
}

export default api;