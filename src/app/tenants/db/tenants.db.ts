import * as mongoose from "mongoose";

const schema = new mongoose.Schema({
  title: {
    type: String,
    default: null,
  },
  dateOfBirth: {
    type: String,
    default: null,
  },
  nationality: {
    type: String,
    default: null,
  },
  numberOfChildren: {
    type: String,
    default: null,
  },
  officeNumber: {
    type: String,
    default: null,
  },
  currentAddress: {
    type: String,
    default: null,
  },
  postalAddress: {
    type: String,
    default: null,
  },
  smoke: {
    type: String,
    default: null,
    enum: ["YES", "NO", null],
  },
  havePets: {
    type: String,
    default: null,
    enum: ["YES", "NO", null],
  },
  email: {
    type: String,
    default: null,
  },
  businessContactName: {
    type: String,
    default: null,
  },
  businessLocation: {
    type: String,
    default: null,
  },
  businessContactNumber: {
    type: String,
    default: null,
  },
  businessEmail: {
    type: String,
    default: null,
  },
  yearsEmployed: {
    type: String,
    default: null,
  },
  firstname: {
    type: String,
    default: null,
  },
  lastname: {
    type: String,
    default: null,
  },
  contactPersonName: {
    type: String,
    default: null,
  },
  contactPersonEmail: {
    type: String,
    default: null,
  },
  contactPersonAddress: {
    type: String,
    default: null,
  },
  contactPersonPhone: {
    type: String,
    default: null,
  },
  contactPersonRelationship: {
    type: String,
    default: null,
  },
  photo: {
    type: String,
    default: null,
  },
  phone: {
    type: String,
    default: null,
  },
  gender: {
    type: String,
    default: null,
    enum: ["Male", "Female"],
  },
  age: {
    type: String,
    default: null,
  },
  married: {
    type: String,
    enum: ["YES", "NO"],
    default: false,
  },
  occupation: {
    type: String,
    default: null,
  },
  password: {
    type: String,
    default: null,
  },
  approved: {
    type: Boolean,
    default: false,
  },
  idNumber: {
    type: String,
    default: null,
  },
  idType: {
    type: String,
    enum: ["voter", "passport", "driverlicence", "ghanacard"],
  },
  blocked: {
    type: Boolean,
    default: false,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model("tenants", schema);
