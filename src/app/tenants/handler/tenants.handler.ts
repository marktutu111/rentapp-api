import { Request, ResponseToolkit } from "@hapi/hapi";
import * as bcrypt from "bcrypt";
import dbTenants from "../db/tenants.db";
import $otp from "../../../otp/handle/otp.handle";
import moment = require("moment");

const forgotPassword = async (request: Request, h: ResponseToolkit) => {
  try {
    const { phone, otp, password } = request.payload as any;
    const tenant = await dbTenants.findOne({
      phone,
      blocked: false,
    });
    if (!tenant || !tenant._id) {
      throw Error("Tenant with phone number does not exit");
    }
    const verified = await $otp.validateOTP({
      phoneNumber: phone,
      otp: otp,
    });
    if (!verified) {
      throw Error("OTP does not match");
    }
    const SALT = await bcrypt.genSalt(10);
    const _hash = await bcrypt.hash(password, SALT);
    await tenant.update({
      password: _hash,
    });
    return h.response({
      success: true,
      message: "Password reset successful",
      data: tenant,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const add = async (request: Request, h: ResponseToolkit) => {
  try {
    const data = request.payload as any;
    const verified = await $otp.validateOTP({
      phoneNumber: data["phone"],
      otp: data["otp"],
    });
    if (!verified) {
      throw Error("OTP does not match");
    }
    const tenant = await dbTenants.findOne({
      phone: data["phone"],
    });
    if (tenant && tenant._id) {
      throw Error("Phone number is already in the system");
    }

    const SALT = await bcrypt.genSalt(10);
    const _hash = await bcrypt.hash(data["password"], SALT);
    const result = await dbTenants.create({
      ...data,
      password: _hash,
    });
    if (!result || !result._id) {
      throw Error("Tenant account failed");
    }
    return h.response({
      success: true,
      message: "Account created successfully, please login to continue.",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const login = async (request: Request, h: ResponseToolkit) => {
  try {
    const { phone, password } = request.payload as any;
    const tenant = await dbTenants.findOne({
      phone: phone,
      blocked: false,
    });
    if (!tenant || !tenant._id) {
      throw Error("Phone number is not found");
    }
    const match = await bcrypt.compare(password, tenant["password"]);
    if (!match) {
      throw Error("Password is not valid");
    }
    return h.response({
      success: true,
      message: "success",
      data: tenant,
      token: "",
      role: "tenant",
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getPhoto = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id } = request.params;
    const result = await dbTenants.findOne({ _id: id }).select({ photo: 1 });
    if (!result || !result._id) {
      throw Error("Tenant not found");
    }
    return h.response({
      success: true,
      message: "success",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getall = async (request: Request, h: ResponseToolkit) => {
  try {
    const results = await dbTenants
      .find({})
      .limit(1000)
      .sort({ createdAt: -1 })
      .select({ photo: 0 });
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const deleteUser = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id } = request.params;
    const results = await dbTenants.findOneAndDelete({
      _id: id,
    });
    if (!results || !results._id) {
      throw Error("User not deleted");
    }
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const filter = async (request: Request, h: ResponseToolkit) => {
  try {
    const { approved, blocked, startDate, endDate } = request.payload as any;
    let query: any = {};
    switch (true) {
      case typeof startDate === "string" && typeof endDate === "string":
        query.createdAt = {
          $gte: moment(startDate).startOf("day"),
          $lte: moment(endDate).endOf("day"),
        };
        break;
      case typeof approved === "boolean":
        query.approved = approved;
        break;
      case typeof blocked === "boolean":
        query.blocked = blocked;
        break;
      default:
        break;
    }
    const result = await dbTenants.find(query).select({ photo: 0 });
    return h.response({
      success: true,
      message: "success",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getTenant = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id } = request.params;
    const result = await dbTenants.findById(id).select({ photo: 0 });
    if (!result || !result._id) {
      throw Error("Tenant not found");
    }
    return h.response({
      success: true,
      message: "success",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getSummary = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id } = request.params;
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const update = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id, data } = request.payload as { id: string; data: any };
    let filter = {};
    Object.keys(data).forEach((key) => {
      if (key && data[key] !== "") {
        filter[key] = data[key];
      }
    });
    const response = await dbTenants.findOneAndUpdate(
      {
        _id: id,
      },
      {
        $set: filter,
      },
      { new: true }
    );
    if (!response || !response._id) {
      throw Error("Account not updated");
    }
    return h.response({
      success: true,
      message: "success",
      data: response,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

export default {
  add,
  update,
  getall,
  login,
  getSummary,
  deleteUser,
  forgotPassword,
  getPhoto,
  getTenant,
  filter,
};
