import db from "./db/tenants.db";
import routes from "./route/tenants.route";

const api={
    pkg:{
        name:'tenants',
        version:'1'
    },
    register:async server=>{
        server.route(routes);
        server.method('dbTenants',db);
    }
}

export default api;