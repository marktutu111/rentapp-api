import * as joi from "joi";
import Handler from "../handler/tenants.handler";
import { ServerRoute } from "@hapi/hapi";

const routes: ServerRoute[] = [
  {
    path: "/filter",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          approved: joi.boolean().optional().allow(null),
          startDate: joi.string().optional().allow(null),
          endDate: joi.string().optional().allow(null),
          blocked: joi.boolean().optional().allow(null),
        }),
      },
    },
    handler: Handler.filter,
  },
  {
    path: "/get/{id}",
    method: "GET",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.getTenant,
  },
  {
    path: "/new",
    method: "POST",
    options: {
      payload: {
        maxBytes: 1000 * 1000 * 5, // 5 Mb
        output: "data",
        parse: true,
      },
    },
    handler: Handler.add,
  },
  {
    path: "/photo/{id}",
    method: "GET",
    handler: Handler.getPhoto,
  },
  {
    path: "/update",
    method: "PUT",
    options: {
      validate: {
        payload: joi.object({
          id: joi.string().required(),
          data: joi.object().required(),
        }),
      },
    },
    handler: Handler.update,
  },
  {
    path: "/resetpassword",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          phone: joi.string().length(10),
          otp: joi.string().required(),
          password: joi.string().required(),
        }),
      },
    },
    handler: Handler.forgotPassword,
  },
  {
    path: "/get",
    method: "GET",
    handler: Handler.getall,
  },
  {
    path: "/login",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          phone: joi.string().required(),
          password: joi.string().required(),
        }),
      },
    },
    handler: Handler.login,
  },
  {
    path: "/delete/{id}",
    method: "DELETE",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.deleteUser,
  },
];

export default routes;
