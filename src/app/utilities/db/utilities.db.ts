import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        type:{
            type:String,
            uppercase:true,
            default:null
        },
        amount:{
            type:Number,
            default:0
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
)

export default mongoose.model(
    'utilities',
    schema
)