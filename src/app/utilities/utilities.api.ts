import db from "./db/utilities.db";
import route from "./route/utilities.route";

const api={
    pkg:{
        name:'package',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbUtilities',db);
        server.route(route);
    }
}

export default api;