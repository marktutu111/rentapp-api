import * as joi from "joi";
import Handler from "../handler/utilities.handler";
import { ServerRoute } from "@hapi/hapi";

const route: ServerRoute[] = [
  {
    path: "/pay",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          utilityId: joi.string().required(),
          tenantId: joi.string().required(),
          accountNumber: joi.string().required(),
          accountIssuer: joi.string().required(),
          paymentType: joi
            .string()
            .default("CASH")
            .required()
            .valid("MOMO")
            .valid("CASH")
            .valid("BANK DEPOSIT"),
          amount: joi.alternatives(joi.string(), joi.number()).required(),
          otp: joi.string().optional(),
          durationStartDate: joi.string().optional(),
          durationEndDate: joi.string().optional(),
          period: joi.string().optional().allow(null).allow(""),
        }),
      },
    },
    handler: Handler.pay,
  },
  {
    path: "/new",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          type: joi.string().required(),
          amount: joi.alternatives(
            joi.string(),
            joi.number()
          ).required(),
        }),
      },
    },
    handler: Handler.newUtility,
  },
  {
    path: "/get",
    method: "GET",
    handler: Handler.utilities,
  },
  {
    path: "/delete/{id}",
    method: "DELETE",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.deleteUtility,
  },
  {
    path: "/update",
    method: "PUT",
    options: {
      validate: {
        payload: joi.object({
          id: joi.string().required(),
          data: joi.object(),
        }),
      },
    },
    handler: Handler.updateHandler,
  },
];

export default route;
