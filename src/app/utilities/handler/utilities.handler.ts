const newUtility = async (request, h) => {
  try {
    const data = request.payload;
    const result = await request.server.methods.dbUtilities.create(data);
    if (!result || !result._id) {
      throw Error("failed");
    }
    return h.response({
      success: true,
      message: "success",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const deleteUtility = async (request, h) => {
  try {
    const { id } = request.params;
    const result = await request.server.methods.dbUtilities.findOneAndRemove({
      _id: id,
    });
    if (!result || !result._id) {
      throw Error("delete failed");
    }
    return h.response({
      success: true,
      message: "success",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const utilities = async (request, h) => {
  try {
    const response = await request.server.methods.dbUtilities
      .find({})
      .limit(1000)
      .sort({ createdAt: -1 });
    return h.response({
      success: true,
      message: "success",
      data: response,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const pay = async (request, h) => {
  try {
    const data = request.payload;
    const utility = await request.server.methods.dbUtilities.findOne({
      _id: data["utilityId"],
    });
    if (!utility || !utility._id) {
      throw Error("No Utility found");
    }
    const verified = await request.server.methods.validateOTP({
      phoneNumber: data["accountNumber"],
      otp: data["otp"],
    });
    if (!verified) {
      throw Error("OTP not valid");
    }
    const payment = await request.server.methods.dbPayments.create({
      tenantId: data["tenantId"],
      utilityId: data["utilityId"],
      purpose: "UTILITY",
      accountNumber: data["accountNumber"],
      accountIssuer: data["accountIssuer"],
      amount: data["amount"],
      paymentType: "MOMO",
      description: `Payment for ${utility["type"]}`,
      property: utility["type"],
      period: data["period"],
    });
    const response = await request.server.methods.processDebit(
      request,
      payment
    );
    return h.response(response);
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const updateHandler = async (request, h) => {
  try {
    const { id, data } = request.payload;
    const response = await request.server.methods.dbUtilities.findOneAndUpdate(
      {
        _id: id,
      },
      {
        $set: data,
      },
      { new: true }
    );
    if (!response || !response._id) {
      throw Error("Update failed");
    }
    return h.response({
      success: true,
      message: "success",
      data: response,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

export default {
  newUtility,
  utilities,
  deleteUtility,
  pay,
  updateHandler,
};
