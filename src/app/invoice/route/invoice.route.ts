import * as joi from "joi";
import Handler from "../handler/invoice.handler";
import { ServerRoute } from "@hapi/hapi";

const route: ServerRoute[] = [
  {
    path: "/add",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          item: joi.string().required(),
          description: joi.string().required(),
          customerName: joi.string().required(),
          customerAddress: joi.string().allow(""),
          customerPhone: joi.string().required(),
          total: joi.alternatives(joi.number(), joi.string()).required(),
          vat: joi.alternatives(joi.number(), joi.string()).required(),
          subtotal: joi.alternatives(joi.number(), joi.string()).required(),
          status: joi.string().required(),
        }),
      },
    },
    handler: Handler.add,
  },
  {
    path: "/get",
    method: "GET",
    handler: Handler.getall,
  },
  {
    path: "/pay",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.pay,
  },
  {
    path: "/summary/get",
    method: "GET",
    handler: Handler.summary,
  },
  {
    path: "/sendmoney",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          id: joi.string().required(),
          accountNumber: joi.string().required(),
          accountIssuer: joi.string().required(),
          amount: joi.string().required(),
        }),
      },
    },
    handler: Handler.sendCash,
  },
];

export default route;
