import * as mongoose from "mongoose";

const schema = new mongoose.Schema({
  customerName: String,
  customerPhone: String,
  customerAddress: String,
  total: Number,
  vat: Number,
  subtotal: Number,
  item: String,
  description: String,
  quantity: String,
  amountPaid: Number,
  status: {
    type: String,
    enum: ["pending", "paid"],
  },
  datePaid: Date,
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model("invoices", schema);
