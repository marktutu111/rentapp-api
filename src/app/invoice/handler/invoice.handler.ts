import { Request, ResponseToolkit } from "hapi";
import * as mongoose from "mongoose";
import * as moment from "moment";
import { formatePhonenumber } from "../../utils/formate-phone-number";

const add = async (request: Request | any, h: ResponseToolkit) => {
  try {
    const data = request.payload;
    const result = await request.server.methods.dbInvoice.create(data);
    if (!result || !result._id) {
      throw Error("Invoice generation failed");
    }
    return h.response({
      success: true,
      message: "Invoice has been recorded successfully",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const pay = async (request: Request | any, h: ResponseToolkit) => {
  try {
    const { id } = request.payload;
    const invoice = await request.server.methods.dbInvoice.findOneAndUpdate(
      {
        _id: id,
        status: "pending",
      },
      {
        $set: {
          status: "paid",
          datePaid: Date.now(),
          updatedAt: Date.now(),
        },
      }
    );
    if (!invoice || !invoice._id) {
      throw Error("Invoice has already been processed");
    }
    return h.response({
      success: true,
      message: "Invoice processed successfully",
      data: invoice,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getall = async (request: Request | any, h: ResponseToolkit) => {
  try {
    const result = await request.server.methods.dbInvoice
      .aggregate([
        {
          $match: {},
        },
        { $sort: { createdAt: -1 } },
        {
          $group: {
            _id: null,
            total: { $sum: "$total" },
            count: { $sum: 1 },
            items: { $push: "$$ROOT" },
          },
        },
      ])
      .sort({ createdAt: -1 });
    return h.response({
      success: true,
      message: "success",
      data: result[0],
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const summary = async (request: Request | any, h: ResponseToolkit) => {
  try {
    const results = await request.server.methods.dbInvoice.aggregate([
      {
        $match: {},
      },
      {
        $facet: {
          today: [
            {
              $match: {
                createdAt: {
                  $gte: moment().startOf("day").toDate(),
                  $lte: moment().endOf("day").toDate(),
                },
              },
            },
            {
              $group: {
                _id: null,
                total: { $sum: "$total" },
                count: { $sum: 1 },
              },
            },
          ],
          week: [
            {
              $match: {
                status: "PAID",
                createdAt: {
                  $gte: moment().startOf("week").toDate(),
                  $lte: moment().endOf("week").toDate(),
                },
              },
            },
            {
              $group: {
                _id: null,
                total: { $sum: "$total" },
                count: { $sum: 1 },
              },
            },
          ],
          month: [
            {
              $match: {
                createdAt: {
                  $gte: moment().startOf("month").toDate(),
                  $lte: moment().endOf("month").toDate(),
                },
              },
            },
            {
              $group: {
                _id: null,
                total: { $sum: "$total" },
                count: { $sum: 1 },
              },
            },
          ],
          year: [
            {
              $match: {
                createdAt: {
                  $gte: moment().startOf("year").toDate(),
                  $lte: moment().endOf("year").toDate(),
                },
              },
            },
            {
              $group: {
                _id: null,
                total: { $sum: "$total" },
                count: { $sum: 1 },
              },
            },
          ],
          all: [
            {
              $group: {
                _id: null,
                amount: { $sum: "$total" },
                count: { $sum: 1 },
              },
            },
          ],
        },
      },
    ]);
    return h.response({
      success: true,
      message: "success",
      data: results[0],
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const sendCash = async (request: Request | any, h: ResponseToolkit) => {
  try {
    const { id, accountNumber, accountIssuer, amount }: any = request.payload;
    const invoice = await request.server.methods.dbInvoice.findOne({
      _id: id,
    });
    if (!invoice || !invoice._id) {
      throw Error("Invoice does not exist");
    }
    const payment: mongoose.Document | any =
      await request.server.methods.dbPayments.create({
        invoiceId: invoice._id,
        purpose: "CASHOUT",
        accountNumber: accountNumber,
        accountIssuer: accountIssuer,
        amount: parseFloat(amount).toFixed(2),
        paymentType: "MOMO",
        description: invoice.description,
      });
    const paymentData: any = {
      amount: payment.amount,
      clienttransid: payment._id,
      nickname: "16th August85 Villa",
      paymentoption: accountIssuer,
      walletnumber: accountNumber,
      clientreference: payment._id,
      description: payment.description,
    };
    const response = await request.server.methods.WigalSendPayment({
      type: "CREDIT",
      payload: paymentData,
    });
    if (response.status !== "OK") {
      throw Error(
        `Sorry we could not process your send money request | ${response.reason}`
      );
    }
    await payment.updateOne({
      transaction: response,
      updatedAt: Date.now(),
      $inc: {
        amountPaid: payment.amount,
      },
    });
    const { customerPhone, customerName, item } = invoice;
    let message =
      "Dear " +
      customerName +
      "," +
      "\n" +
      "Your have received an amount of GHS" +
      amount +
      " " +
      "as part or full payment for " +
      item +
      " ." +
      "\n" +
      "16th August85,Villa. Thank You";
    request.server.methods
      .sendsmsArkesel({
        message: message,
        destination: formatePhonenumber(customerPhone),
      })
      .catch((err) => null);
    return h.response({
      success: true,
      message: "Your payment transaction has been completed successfully",
      data: payment,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

export default {
  add,
  getall,
  pay,
  summary,
  sendCash,
};
