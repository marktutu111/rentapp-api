import { Request, ResponseToolkit } from "hapi";
import { Document } from "mongoose";
import { formatePhonenumber } from "../../utils/formate-phone-number";
import dbSettings from "../db/settings.db";

const deleteSettings = async (request, h) => {
  try {
    const { id } = request.params;
    const result = await dbSettings.Settings.findOneAndRemove({
      _id: id,
    });
    if (!result || !result._id) {
      throw Error("Settings could not be saved");
    }
    return h.response({
      success: true,
      message: "Settings deleted successfully",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const updateSettings = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id, data } = request.payload as any;
    const result = await dbSettings.Settings.findOneAndUpdate(
      { _id: id },
      { $set: { ...data, updatedAt: Date.now() } },
      { new: true }
    );
    if (!result || !result._id) {
      throw Error("update failed");
    }
    return h.response({
      success: true,
      message: "success",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const addSettings = async (request: Request, h: ResponseToolkit) => {
  try {
    const payload: any = request.payload;
    let result = await dbSettings.Settings.findOne({});
    if (!result || !result._id) {
      result = await dbSettings.Settings.create(payload);
    } else {
      await result.update(payload);
    }
    return h.response({
      success: true,
      message: "Settings updated successfully",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const loadSettingAll = async (request: Request, h: ResponseToolkit) => {
  try {
    const results = await dbSettings.Settings.find({});
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const loadSettings = async (request: Request, h: ResponseToolkit) => {
  try {
    const results = await dbSettings.Settings.findOne({});
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const pushSms = async (request: Request | any, h: ResponseToolkit) => {
  try {
    const { message }: any = request.payload;
    const tenants: Document[] = await request.server.methods.dbTenants.find({});
    if (tenants.length > 0) {
      await dbSettings.Sms.create({
        message: message,
      });
    }
    tenants.forEach((tenant: any) => {
      request.server.methods
        .sendSMS({
          message: message,
          destination: formatePhonenumber(tenant.phone),
        })
        .catch((err) => null);
    });
    return h.response({
      success: true,
      message: "Your message has been successfully sent",
      data: tenants,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getSms = async (request: Request, h: ResponseToolkit) => {
  try {
    const results = await dbSettings.Sms.find({})
      .sort({ createdAt: -1 })
      .limit(100);
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

export default {
  pushSms,
  getSms,
  addSettings,
  loadSettings,
  loadSettingAll,
  deleteSettings,
  updateSettings,
};
