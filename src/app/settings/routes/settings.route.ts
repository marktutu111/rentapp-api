import { ServerRoute } from "hapi";
import * as joi from "joi";
import Handler from "../handler/settings.handler";

const routes: ServerRoute[] = [
  {
    path: "/get",
    method: "GET",
    handler: Handler.loadSettings,
  },
  {
    path: "/get/all",
    method: "GET",
    handler: Handler.loadSettingAll,
  },
  {
    path: "/add",
    method: "PUT",
    handler: Handler.addSettings,
  },
  {
    path: "/update",
    method: "PUT",
    options: {
      validate: {
        payload: joi.object({
          id: joi.string().required(),
          data: joi.object().required(),
        }),
      },
    },
    handler: Handler.updateSettings,
  },
  {
    path: "/sms/push",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          message: joi.string().required(),
        }),
      },
    },
    handler: Handler.pushSms,
  },
  {
    path: "/sms/get",
    method: "GET",
    handler: Handler.getSms,
  },
  {
    path: "/delete/{id}",
    method: "DELETE",
    handler: Handler.deleteSettings,
  },
];

export default routes;
