import { Server } from "hapi";
import route from "./routes/settings.route";
import db from "./db/settings.db";

const api = {
  name: "settings",
  version: "1",
  register: async (server: any) => {
    server.method("dbSettings", db.Settings);
    server.route(route);
  },
};

export default api;
