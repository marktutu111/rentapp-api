import * as mongoose from "mongoose";

const settings = new mongoose.Schema({
  dollarRate: {
    type: Number,
    default: 0,
  },
  smsBalance: {
    type: Number,
    default: 0,
  },
  receive_sms_alerts: {
    type: [String],
    default: [],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

const sms = new mongoose.Schema({
  message: String,
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const Settings = mongoose.model("settings", settings);
const Sms = mongoose.model("sms", sms);
export default {
  Sms,
  Settings,
};
