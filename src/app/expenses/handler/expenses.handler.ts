import { Request, ResponseToolkit } from "hapi";
import *as moment from "moment";
import *as mongoose from "mongoose";
import dbExpenses from "../db/expenses.db";


const addExpense=async(request,h)=>{
    try {
        const data=request.payload;
        const result=await request.server.methods.dbExpenses.create(data);
        if(!result || !result._id){
            throw Error(
                'Expense not added'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:data
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getExpenses=async(request,h)=>{
    try {
        const results=await request.server.methods.dbExpenses.aggregate(
            [
                { $match:{}},
                { $sort:{ createdAt:-1 } },
                { "$lookup":{from:'expacnts',localField:'category', foreignField:'_id', as:'category'}},
                { $unwind:{ path:'$category', preserveNullAndEmptyArrays: true } },
                {
                    "$group": {
                        "_id":null, 
                        "total":{$sum:"$amount"},
                        "count":{$sum:1},
                        "transactions":{ $push:'$$ROOT' }
                    }
                }
            ]
        )
        return h.response(
            {
                success:true,
                message:'success',
                data:results[0]
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const addCategory=async(request,h)=>{
    try {
        const data=request.payload;
        const result=await request.server.methods.dbExpenseCategory.create(data);
        if(!result || !result._id){
            throw Error(
                'Category add failed'
            )
        };
        return h.response(
            {
                success:true,
                message:'success',
                data:result
            }
        )
    } catch (err) {
        console.log(err);
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const getCategories=async(request,h)=>{
    try {
        const results=await request.server.methods.dbExpenseCategory.find(
            {}
        ).sort({createdAt:-1});
        return h.response(
            {
                success:true,
                message:'success',
                data:results
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const deleteExpense=async(request,h:ResponseToolkit)=>{
    try {
        const ids=request.payload;
        let result=await dbExpenses.expenses.deleteMany(
            {
                _id:{ $in:ids }
            }
        )
        if(result && result.deletedCount<=0){
            throw Error(
                'Oops expense delete failed'
            )
        };
        return h.response(
            {
                success:true,
                message:'Expense deleted successfully',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
};



const filterExpenses=async(request,h)=>{
    try {
        const {start,end,account}=request.query;
        let _start=moment(new Date(start || Date.now())).startOf('day').toDate(),
            _end=moment(new Date(end || Date.now())).endOf('day').toDate(),
            query:any={
                createdAt:{
                    $gte:_start,
                    $lte:_end
                }
            };

        if(mongoose.Types.ObjectId.isValid(account)){
            query={
                ...query,
                category:new mongoose.Types.ObjectId(account)
            }
        }
        console.log(query);
        
        const result=await request.server.methods.dbExpenses.aggregate(
            [
                {
                    $match:query
                },
                { $sort:{ createdAt:-1 } },
                { "$lookup":{from:'expacnts',localField:'category', foreignField:'_id', as:'category'}},
                { $unwind:{ path:'$category', preserveNullAndEmptyArrays: true } },
                {
                    "$group": {
                        "_id":null, 
                        "total":{$sum:"$amount"},
                        "count":{$sum:1},
                        "transactions":{ $push:'$$ROOT' }
                    }
                }
            ]
        )
        return h.response(
            {
                success:true,
                message:'success',
                data:result[0]
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}




const deleteCategory=async(request:Request|any,h:ResponseToolkit)=>{
    try {
        const ids=request.payload;
        let result=await request.server.methods.dbExpenses.findOne(
            {
                category:{$in:ids}
            }
        )
        if(result && result._id){
            throw Error(
                'Expence category cannot be deleted because it is linked to an Expense record'
            )
        };
        result=await request.server.methods.dbExpenseCategory.deleteMany(
            {
                _id:{$in:ids}
            }
        );
        if(result && result.deletedCount<=0){
            throw Error(
                'Oops something went wrong, account not deleted'
            )
        };
        return h.response(
            {
                success:true,
                message:'Expense account deleted successfully',
                data:result
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
}


const summary=async(request:Request|any,h:ResponseToolkit)=>{
    try {
        const results=await request.server.methods.dbExpenses.aggregate(
            [
                {
                    $match:{}
                },
                {
                    $facet:{
                        today:[
                            {
                                $match:{
                                    createdAt:{
                                        $gte:moment().startOf('day').toDate(),
                                        $lte:moment().endOf('day').toDate()
                                    }
                                }
                            },
                            {
                                "$group": {
                                    "_id":null, 
                                    "total":{$sum:"$amount"},
                                    "count":{$sum:1}
                                }
                            }
                        ],
                        week:[
                            {
                                $match:{
                                    status:'PAID',
                                    createdAt:{
                                        $gte:moment().startOf('week').toDate(),
                                        $lte:moment().endOf('week').toDate()
                                    }
                                }
                            },
                            {
                                "$group": {
                                    "_id":null, 
                                    "total":{$sum:"$amount"},
                                    "count":{$sum:1}
                                }
                            }
                        ],
                        month:[
                            {
                                $match:{
                                    createdAt:{
                                        $gte:moment().startOf('month').toDate(),
                                        $lte:moment().endOf('month').toDate()
                                    }
                                }
                            },
                            {
                                "$group": {
                                    "_id":null, 
                                    "total":{$sum:"$amount"},
                                    "count":{$sum:1}
                                }
                            }
                        ],
                        year:[
                            {
                                $match:{
                                    createdAt:{
                                        $gte:moment().startOf('year').toDate(),
                                        $lte:moment().endOf('year').toDate()
                                    }
                                }
                            },
                            {
                                "$group": {
                                    "_id":null, 
                                    "total":{$sum:"$amount"},
                                    "count":{$sum:1}
                                }
                            }
                        ],
                        all:[
                            {
                                "$group": {
                                    "_id":null, 
                                    "amount":{$sum:"$amount"},
                                    "count":{$sum:1}
                                }
                            },
                        ]
                    }
                }
            ]
        );
        return h.response(
            {
                success:true,
                message:'success',
                data:results[0]
            }
        )
    } catch (err) {
        return h.response(
            {
                success:false,
                message:err.message
            }
        )
    }
};


export default {
    addExpense,
    getExpenses,
    addCategory,
    getCategories,
    deleteCategory,
    deleteExpense,
    filterExpenses,
    summary
}