import * as joi from "joi";
import Handler from "../handler/expenses.handler";
import { ServerRoute } from "@hapi/hapi";

const route: ServerRoute[] = [
  {
    path: "/category",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          name: joi.string().required(),
          description: joi.string().optional(),
        }),
      },
    },
    handler: Handler.addCategory,
  },
  {
    path: "/get",
    method: "GET",
    handler: Handler.getExpenses,
  },
  {
    path: "/filter",
    method: "GET",
    options: {
      validate: {
        query: joi.object({
          start: joi.string().optional(),
          end: joi.string().optional(),
          account: joi.string().optional(),
        }),
      },
    },
    handler: Handler.filterExpenses,
  },
  {
    path: "/category/get",
    method: "GET",
    handler: Handler.getCategories,
  },
  {
    path: "/category/delete",
    method: "POST",
    options: {
      validate: {
        payload: joi.array(),
      },
    },
    handler: Handler.deleteCategory,
  },
  {
    path: "/delete",
    method: "POST",
    options: {
      validate: {
        payload: joi.array(),
      },
    },
    handler: Handler.deleteExpense,
  },
  {
    path: "/add",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          category: joi.string().required(),
          expenseDate: joi.string().required(),
          amount: joi.alternatives(joi.string(), joi.number()).required(),
          tax: joi
            .alternatives(joi.string(), joi.number())
            .optional()
            .allow(null)
            .default(0),
          reference: joi.string().optional().allow(null),
          notes: joi.string().optional().allow(null),
        }),
      },
    },
    handler: Handler.addExpense,
  },
  {
    path: "/summary/get",
    method: "GET",
    handler: Handler.summary,
  },
];

export default route;
