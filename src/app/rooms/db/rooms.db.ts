import * as mongoose from "mongoose";

const schema = new mongoose.Schema({
  type: {
    type: String,
    default: null,
  },
  location: {
    type: String,
    default: null,
  },
  address: {
    type: String,
    default: null,
  },
  size: {
    type: String,
    default: null,
  },
  condition: {
    type: String,
    default: null,
  },
  furnishing: {
    type: String,
    default: null,
  },
  roomNumber: {
    type: String,
    default: null,
  },
  price: {
    type: Number,
    default: 0,
  },
  details: {
    type: String,
    default: null,
  },
  occupied: {
    type: Boolean,
    default: false,
  },
  images: {
    type: [String],
    default: [],
  },
  accomodation: {
    type: String,
    default: "LONG STAY",
    uppercase: true,
    enum: ["SHORT STAY", "LONG STAY"],
  },
  inspectionAmount: {
    type: Number,
    default: 0,
  },
  currency: {
    type: String,
    default: "GHS",
    enum: ["GHS", "USD"],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model("rooms", schema);
