import db from "./db/rooms.db";
import routes from "./routes/rooms.route";

const api={
    pkg:{
        name:'rooms',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbRooms',db);
        server.route(routes);
    }
}

export default api;