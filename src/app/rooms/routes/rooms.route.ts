import * as joi from "joi";
import Handler from "../handler/rooms.handler";
import { ServerRoute } from "@hapi/hapi";

const routes: ServerRoute[] = [
  {
    path: "/new",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          type: joi.string().required(),
          location: joi.string().required(),
          address: joi.string().required(),
          size: joi.string().required(),
          condition: joi.string().required(),
          furnishing: joi.string().required(),
          roomNumber: joi.string().required(),
          price: joi.string().required(),
          details: joi.string().required(),
          currency: joi
            .string()
            .optional()
            .default("GHS")
            .valid("GHS")
            .valid("USD"),
          inspectionAmount: joi
            .alternatives(joi.string(), joi.number())
            .optional()
            .default(0),
          accomodation: joi
            .string()
            .required()
            .valid("LONG STAY")
            .valid("SHORT STAY")
            .default("LONG STAY")
        }),
      },
    },
    handler: Handler.addRoom,
  },
  {
    path: "/get",
    method: "GET",
    handler: Handler.getRooms,
  },
  {
    path: "/upload/image",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          id: joi.string().required(),
          images: joi.array(),
        }),
      },
    },
    handler: Handler.saveImage,
  },
  {
    path: "/all/get",
    method: "GET",
    handler: Handler.getAllRooms,
  },
  {
    path: "/delete/{id}",
    method: "DELETE",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.deleteRoom,
  },
  {
    path: "/images/get/{id}",
    method: "GET",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.getImages,
  },
  {
    path: "/image/delete/{id}",
    method: "DELETE",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.deleteImage,
  },
  {
    path: "/update",
    method: "PUT",
    options: {
      validate: {
        payload: joi.object({
          id: joi.string().required(),
          data: joi.object().required(),
        }),
      },
    },
    handler: Handler.update,
  },
];

export default routes;
