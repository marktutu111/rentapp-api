import { ResponseToolkit } from "@hapi/hapi";
import { Request } from "hapi";
import * as mongoose from "mongoose";
import dbStorage from "../../storage/storage.db";
import dbRooms from "../db/rooms.db";

const deleteImage = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id } = request.params;
    const image = await dbStorage.findOneAndRemove({ _id: id });
    if (!image || !image._id) {
      throw Error("Image not found");
    }
    return h.response({
      success: true,
      message: "Image successfully deleted",
      data: image,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: (err as Error)?.message,
    });
  }
};

const saveImage = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id, images } = request.payload as {
      id: mongoose.Types.ObjectId;
      images: Array<{ data: string; type: string }>;
    };
    for (const img of images) {
      const { type, data } = img;
      await dbStorage.create({
        id,
        type,
        data,
        name: "Property image",
      });
    }
    return h.response({
      success: true,
      message: "Images uploaded successfully",
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const addRoom = async (request: Request, h: ResponseToolkit) => {
  try {
    const data = request.payload as any;
    const room = await dbRooms.create(data);
    if (!room || !room._id) {
      throw Error("Oops something went wrong");
    }
    return h.response({
      success: true,
      message: "Room added successfully",
      data: room,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const deleteRoom = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id } = request.params;
    const room = await dbRooms.findOneAndRemove({
      _id: id,
    });
    if (!room || !room._id) {
      throw Error("Room not found");
    }
    await dbStorage.deleteMany({ id });
    return h.response({
      success: true,
      message: "Room deleted",
      data: room,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getRooms = async (request: Request, h: ResponseToolkit) => {
  try {
    const rooms = await dbRooms
      .find({
        occupied: false,
      })
      .limit(1000)
      .sort({ createdAt: -1 });
    return h.response({
      success: true,
      message: "success",
      data: rooms,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getAllRooms = async (request: Request, h: ResponseToolkit) => {
  try {
    const rooms = await dbRooms.find({}).limit(1000).sort({ createdAt: -1 });
    return h.response({
      success: true,
      message: "success",
      data: rooms,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getImages = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id } = request.params;
    const images = await dbStorage.find({ id });
    return h.response({
      success: true,
      message: "success",
      data: images,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const update = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id, data } = request.payload as any;
    let filter = {};
    Object.keys(data).forEach((key) => {
      if (key && data[key] !== "") {
        filter[key] = data[key];
      }
    });
    const response = await dbRooms.findOneAndUpdate(
      { _id: id },
      { $set: filter },
      { new: true }
    );
    if (!response || !response._id) {
      throw Error("Property update failed");
    }
    return h.response({
      success: true,
      message: "success",
      data: response,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

export default {
  addRoom,
  deleteRoom,
  getRooms,
  getImages,
  update,
  getAllRooms,
  saveImage,
  deleteImage,
};
