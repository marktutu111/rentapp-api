export type TPaymentStatus = "PENDING" | "FAILED" | "PAID" | "REVERSED";

export enum EnumPaymentStatus {
  "PENDING" = "PENDING",
  "FAILED" = "FAILED",
  "PAID" = "PAID",
  "REVERSED" = "REVERSED",
  "INITIATED"="INITIATED"
}

export enum EnumOperators {
  "REDDE" = "REDDE",
  "PPAY" = "PPAY",
  "LAZYPAY" = "LAZYPAY",
}

export enum EPaymentPurpose {
  "CASHOUT" = "CASHOUT",
  "RENEWAL" = "RENEWAL", // rent renewal
  "RENT" = "RENT",
  "INSPECTION" = "INSPECTION",
  "UTILITY" = "UTILITY",
  "SD" = "SD", // security deposit
}
