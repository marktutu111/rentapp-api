import { Request } from "hapi";

const processAmount = async ({
  request,
  amount,
  room,
}): Promise<{ convertedAmount: number; currency: string; price: number }> => {
  try {
    const [settings, roomDetails] = await Promise.all([
      request.server.methods.dbSettings.findOne({}),
      request.server.methods.dbRooms.findOne({ _id: room }),
    ]);
    let convertedAmount: number = 0;
    const { dollarRate } = settings;
    const { currency, price } = roomDetails;
    switch (currency) {
      case "GHS":
        convertedAmount = amount;
        break;
      case "USD":
        convertedAmount = parseFloat(amount) * dollarRate;
        break;
      default:
        break;
    }
    if (convertedAmount <= 0) {
      throw Error("Invalid Amount provided");
    }
    convertedAmount = parseFloat(
      parseFloat(convertedAmount.toString()).toFixed(0)
    );
    return {
      convertedAmount,
      currency,
      price,
    };
  } catch (err) {
    return err;
  }
};

export default processAmount;
