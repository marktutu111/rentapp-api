import { Request, ResponseToolkit } from "@hapi/hapi";
import * as moment from "moment";
import dbRents from "../db/rents.db";
import dbRooms from "../../rooms/db/rooms.db";
import dbPayments from "../../payments/db/payments.db";
import * as mongoose from "mongoose";

const newRent = async (request, h) => {
  try {
    const {
      tenant,
      room,
      accountNumber,
      accountIssuer,
      amount,
      paymentType,
      duration,
      costPerMonth,
      otp,
    } = request.payload;
    const { convertedAmount, currency } =
      await request.server.methods.processAmount({
        request,
        amount,
        room,
      });
    const rent = await request.server.methods.dbRents.findOne({
      tenant: tenant,
      room: room,
    });
    if (rent && rent._id) {
      throw Error("This tenant is already occuppying this space");
    }
    const result = await request.server.methods.dbRents.create({
      room: room,
      tenant: tenant,
      duration: duration,
      durationType: "months",
      costPerMonth: costPerMonth,
      paymentType: paymentType,
      currency: currency,
      amount: convertedAmount,
    });
    if (!result || !result._id) {
      throw Error("Oops, something went wrong");
    }
    if (paymentType === "MOMO") {
      const payment = await request.server.methods.dbPayments.create({
        tenantId: tenant,
        rentId: result["_id"],
        purpose: "RENT",
        accountNumber: accountNumber,
        accountIssuer: accountIssuer,
        amount: result.amount,
        accountType: "momo",
        paymentType: paymentType,
      });
      const verified = await request.server.methods.validateOTP({
        phoneNumber: accountNumber,
        otp: otp,
      });
      if (!verified) {
        throw Error("OTP does not match");
      }
      const response = await request.server.methods.processDebit(
        request,
        payment
      );
      return h.response(response);
    }
    return h.response({
      success: true,
      message:
        "Your reservation request has been processed please make payment to complete your request",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const adminAddRent = async (request, h) => {
  try {
    const {
      tenant,
      room,
      paymentType,
      duration,
      costPerMonth,
      amount,
      rentType,
    } = request.payload;
    const rent = await request.server.methods.dbRents.findOne({
      tenant: tenant,
      room: room,
    });
    if (rent && rent._id && rentType === "NEW") {
      throw Error(
        "This tenant is already occuppying this space,you can set the type to RENEWAL for the tenant"
      );
    }
    const result = await request.server.methods.dbRents.create({
      room: room,
      tenant: tenant,
      duration: duration,
      durationType: "months",
      costPerMonth: costPerMonth,
      paymentType: paymentType,
      amount: amount,
      rentType: rentType,
    });
    if (!result || !result._id) {
      throw Error("Oops, something went wrong");
    }
    return h.response({
      success: true,
      message: "Your reservation request has been processed successfully",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const approveRent = async (request: Request, h: ResponseToolkit) => {
  try {
    const { tenantId, rentId, startDate, endDate } = request.payload as any;
    const result = await dbRents.findOne({
      _id: rentId,
      tenant: tenantId,
    });
    if (!result || !result._id) {
      throw Error(
        "Request could not be completed, We are sorry no matching rent found"
      );
    }
    await result.updateOne({
      $set: {
        startDate: startDate,
        endDate: endDate,
        status: "ACTIVE",
      },
    });
    const room = await dbRooms.findOneAndUpdate(
      { _id: result.room },
      {
        $set: {
          occupied: true,
          updatedAt: Date.now(),
        },
      }
    );
    switch (result.status) {
      case "PENDING PAYMENT":
        const { amount, paymentType } = result;
        await dbPayments.findOneAndUpdate(
          {
            rentId,
            tenantId,
            purpose: "RENT",
          },
          {
            $set: {
              property: room.type,
              amount: amount,
              paymentType: paymentType,
              status: "PAID",
              description: `Payment made for property ${room["type"]}`,
              datePaid: Date.now(),
            },
          },
          { upsert: true }
        );
        break;
      default:
        break;
    }
    return h.response({
      success: true,
      message: "rent approved successfully",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const filter = async (request: Request, h: ResponseToolkit) => {
  try {
    const { room, accomodation, status, startDate, endDate } =
      request.payload as {
        room: mongoose.Types.ObjectId;
        accomodation: string;
        status: string;
        startDate: string;
        endDate: string;
      };
    let query: any = {};
    switch (true) {
      case typeof startDate === "string" && typeof endDate === "string":
        query.startDate = { $gte: moment(startDate).startOf("day").toDate() };
        query.endDate = { $lte: moment(endDate).endOf("day").toDate() };
        break;
      case mongoose.Types.ObjectId.isValid(room):
        query.room = room;
        break;
      case typeof accomodation === "string":
        query.accomodation = accomodation;
        break;
      case typeof status === "string":
        query.status = status;
        break;
      default:
        break;
    }
    const results = await dbRents
      .find(query)
      .sort({ createdAt: -1 })
      .populate("tenant", { photo: 0 })
      .populate("room");
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const update = async (request, h) => {
  try {
    const { id, data } = request.payload;
    let filter = {};
    Object.keys(data).forEach((key) => {
      if (key && data[key]) {
        filter[key] = data[key];
      }
    });
    const response = await dbRents.findOneAndUpdate(
      { _id: id },
      { $set: filter },
      { new: true }
    );
    if (!response || !response._id) {
      throw Error("Rent not updated");
    }
    return h.response({
      success: true,
      message: "success",
      data: response,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getRentById = async (request: Request, h: ResponseToolkit) => {
  try {
    const { id } = request.params as { id: mongoose.Types.ObjectId };
    const results = await dbRents
      .findById(id)
      .populate("tenant", { photo: 0 })
      .populate("room");
    if (!results || !results._id) {
      throw Error("Rent not found");
    }
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getall = async (request, h) => {
  try {
    const results = await dbRents
      .find({})
      .limit(1000)
      .sort({ createdAt: -1 })
      .populate("tenant", { photo: 0 })
      .populate("room");
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const getPending = async (request, h) => {
  try {
    const results = await request.server.methods.dbRents
      .find({
        $or: [
          {
            status: "PENDING APPROVAL",
          },
          {
            status: "PENDING PAYMENT",
          },
        ],
      })
      .limit(1000)
      .sort({ createdAt: -1 })
      .populate("tenant")
      .populate("room");
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: true,
      message: err.message,
    });
  }
};

const getTenantPendingRents = async (request, h: ResponseToolkit) => {
  try {
    const { id } = request.params;
    const results = await request.server.methods.dbRents
      .find({
        status: "PENDING APPROVAL",
        tenant: id,
      })
      .sort({ createdAt: -1 })
      .populate("tenant")
      .populate("room");
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: true,
      message: err.message,
    });
  }
};

const getTenantRents = async (request, h) => {
  try {
    const { id } = request.params;
    const results = await request.server.methods.dbRents
      .find({
        tenant: id,
      })
      .limit(1000)
      .sort({ createdAt: -1 })
      .populate("tenant")
      .populate("room");
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: true,
      message: err.message,
    });
  }
};

const rentDetails = async (request, h) => {
  try {
    const { id } = request.params;
    const response = await request.server.methods.dbRents
      .findOne({
        _id: id,
      })
      .populate("room")
      .populate("tenant");
    return h.response({
      success: true,
      message: "success",
      data: response,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const revoke = async (request, h) => {
  try {
    const { rentId, tenantId, reason } = request.payload;
    const rent = await request.server.methods.dbRents.findOneAndUpdate(
      {
        _id: rentId,
        tenant: tenantId,
      },
      {
        $set: {
          status: "REVOKED",
          revokedReason: reason,
        },
      },
      { new: true }
    );
    if (!rent || !rent._id) {
      throw Error("Sorry request could not be processed");
    }
    const { endDate, costPerMonth, room } = rent;
    const durationRemaining: number = moment().diff(
      new Date(endDate),
      "months"
    );
    const balanceRemaing = durationRemaining * costPerMonth;
    await rent.update({
      durationRemaining: durationRemaining,
      balanceRemaing: balanceRemaing,
      dateMoved: Date.now(),
    });
    request.server.methods.dbRooms
      .findOneAndUpdate(
        {
          _id: room,
        },
        {
          $set: {
            occupied: false,
          },
        }
      )
      .catch((err) => null);
    return h.response({
      success: true,
      message: "Tenant removed from property successfully",
      data: rent,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const securityDeposit = async (request, h) => {
  try {
    const {
      propertyId,
      paymentType,
      otp,
      accountNumber,
      accountIssuer,
      amount,
    } = request.payload;
    const property = await request.server.methods.dbRooms.findOne({
      _id: propertyId,
      occupied: false,
    });
    if (!property || !property._id) {
      throw Error("Property not available");
    }
    if (paymentType === "MOMO") {
      const verified = await request.server.methods.validateOTP({
        phoneNumber: accountNumber,
        otp: otp,
      });
      if (!verified) {
        throw Error("OTP not valid");
      }
    }
    const payment = await request.server.methods.dbPayments.create({
      purpose: "SD",
      accountNumber: accountNumber,
      accountIssuer: accountIssuer,
      amount: amount,
      paymentType: paymentType,
      description: `Payment for securty deposit`,
      property: property["type"],
      propertyId: propertyId,
    });
    const response = await request.server.methods.processDebit(
      request,
      payment
    );
    return h.response(response);
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const renewRent = async (request, h) => {
  try {
    const {
      rent,
      paymentType,
      otp,
      duration,
      accountNumber,
      accountIssuer,
      startDate,
      amount,
    } = request.payload;
    const _rent = await request.server.methods.dbRents
      .findOne({
        _id: rent,
      })
      .populate("room");
    if (!_rent || !_rent._id) {
      throw Error(
        "Sorry we couldn't find any matching rent for your renewal request"
      );
    }
    const _checkPending = await request.server.methods.dbRents.findOne({
      tenant: _rent.tenant,
      $or: [
        {
          status: "PENDING APPROVAL",
        },
        {
          status: "PENDING PAYMENT",
        },
      ],
    });
    if (_checkPending && _checkPending._id) {
      throw Error(
        "Sorry we couldn't find any matching rent for your renewal request,you have rent pending approval or payment which needs to be processed.kindly contact your landlord."
      );
    }

    const { tenant, room, accomodation } = _rent;

    const { convertedAmount, currency, price } =
      await request.server.methods.processAmount({
        request,
        amount,
        room: room._id,
      });

    const _endDate = moment(new Date(startDate))
      .add(duration, "months")
      .toDate();
    const payment = await request.server.methods.dbPayments.create({
      tenantId: tenant,
      rentId: _rent["_id"],
      purpose: "RENEWAL",
      accountNumber: accountNumber,
      accountIssuer: accountIssuer,
      amount: convertedAmount,
      paymentType: paymentType,
      description: `Renewal payment for ${room["type"]}`,
      property: room["type"],
      propertyId: room["_id"],
      durationStartDate: startDate,
      durationEndDate: _endDate,
      duration: duration,
    });
    switch (paymentType) {
      case "MOMO":
        payment
          .update({
            accountType: "momo",
          })
          .catch((err) => null);
        const verified = await request.server.methods.validateOTP({
          phoneNumber: accountNumber,
          otp: otp,
        });
        if (!verified) {
          throw Error("OTP does not match");
        }
        const response = await request.server.methods.processDebit(
          request,
          payment
        );
        return h.response(response);
      default:
        await payment.update({
          status: "PAID",
        });
        const result = await request.server.methods.dbRents.create({
          room: room._id,
          tenant: tenant,
          duration: duration,
          durationType: "months",
          costPerMonth: price,
          paymentType: paymentType,
          amount: convertedAmount,
          rentType: "RENEWAL",
          accomodation: accomodation,
          status: "PENDING APPROVAL",
          startDate: startDate,
          endDate: _endDate,
          currency: currency,
        });
        if (!result || !result._id) {
          throw Error(
            "Oops something went wrong we couldn't process your request"
          );
        }
        return h.response({
          success: true,
          message:
            "Your payment request has been processed, your landlord will confirm and approve request",
          data: result,
        });
    }
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
      data: null,
    });
  }
};

const pay = async (request, h) => {
  try {
    const { rent, paymentType, otp, accountNumber, accountIssuer, amount } =
      request.payload;
    let result = await request.server.methods.dbRents
      .findOne({
        _id: rent,
        $or: [
          {
            status: "PENDING PAYMENT",
          },
          {
            status: "PENDING APPROVAL",
          },
        ],
      })
      .populate("room");
    if (!result || !result._id) {
      throw Error(
        "Sorry we couldn't find any matching property for your request.if you want to make payment for renewal,kindly contact your landloard to create this request for you and make payment."
      );
    }
    const { tenant, room } = result;

    const { convertedAmount } = await request.server.methods.processAmount({
      request,
      amount,
      room: room._id,
    });

    const payment = await request.server.methods.dbPayments.create({
      tenantId: tenant,
      rentId: result["_id"],
      purpose: "RENT",
      accountNumber: accountNumber,
      accountIssuer: accountIssuer,
      amount: convertedAmount,
      paymentType: paymentType,
      description: `Payment for ${room["type"]}`,
      property: room["type"],
    });
    if (paymentType === "MOMO") {
      payment
        .update({
          accountType: "momo",
        })
        .catch((err) => null);
      const verified = await request.server.methods.validateOTP({
        phoneNumber: accountNumber,
        otp: otp,
      });
      if (!verified) {
        throw Error("OTP does not match");
      }
      const response = await request.server.methods.processDebit(
        request,
        payment
      );
      return h.response(response);
    }
    return h.response({
      success: true,
      message:
        "Your payment request has been processed, your landlord will confirm and approve request",
      data: result,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const deleteRent = async (request, h) => {
  try {
    const { id } = request.params;
    const results = await request.server.methods.dbRents.findOneAndDelete({
      _id: id,
    });
    if (!results || !results._id) {
      throw Error("Rent not deleted");
    }
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

export default {
  newRent,
  update,
  getall,
  approveRent,
  getTenantRents,
  rentDetails,
  pay,
  getPending,
  revoke,
  adminAddRent,
  deleteRent,
  securityDeposit,
  renewRent,
  getTenantPendingRents,
  getRentById,
  filter
};
