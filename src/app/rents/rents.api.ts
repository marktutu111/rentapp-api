import db from "./db/rents.db";
import routes from "./routes/rents.route";
import processAmount from "./handler/pamount.handler";
import { Server } from "hapi";

const api={
    pkg:{
        name:'rents',
        version:'1'
    },
    register:async(server:Server|any)=>{
        server.method('processAmount',processAmount);
        server.method('dbRents',db);
        server.route(routes);
    }
}

export default api;