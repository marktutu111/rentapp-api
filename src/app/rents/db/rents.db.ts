import * as mongoose from "mongoose";

export enum ERentStatus {
  "DUE" = "DUE",
  "PENDING APPROVAL" = "PENDING APPROVAL",
  "EXPIRED" = "EXPIRED",
  "ACTIVE" = "ACTIVE",
  "REVOKED" = "REVOKED",
  "PENDING PAYMENT" = "PENDING PAYMENT",
}

const schema = new mongoose.Schema({
  room: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "rooms",
  },
  tenant: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "tenants",
  },
  duration: {
    type: Number,
    default: 0,
  },
  currency: String,
  paymentType: {
    type: String,
    default: "CASH",
    enum: ["CASH", "MOMO", "BANK DEPOSIT"],
  },
  amount: {
    type: Number,
    default: 0,
  },
  durationType: {
    type: String,
    default: "months",
    enum: ["months", "years"],
  },
  status: {
    type: String,
    default: ERentStatus["PENDING PAYMENT"],
    uppercase: true,
    enum: ERentStatus,
  },
  startDate: Date,
  endDate: Date,
  costPerMonth: {
    type: Number,
    default: 0,
  },
  rentType: {
    type: String,
    default: "NEW",
    enum: ["NEW", "RENEWAL"],
  },
  durationRemaining: Number,
  balanceRemaing: Number,
  revokedReason: String,
  dateMoved: Date,
  accomodation: {
    type: String,
    default: "LONG STAY",
    enum: ["SHORT STAY", "LONG STAY"],
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  updatedAt: {
    type: Date,
    default: Date.now,
  },
});

export default mongoose.model("rents", schema);
