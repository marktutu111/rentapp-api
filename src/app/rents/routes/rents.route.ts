import * as joi from "joi";
import Handler from "../handler/rents.handler";
import { ServerRoute } from "@hapi/hapi";

const routes: ServerRoute[] = [
  {
    path: "/new",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          room: joi.string().required(),
          tenant: joi.string().required(),
          duration: joi.alternatives(joi.string(), joi.number()),
          durationType: joi
            .string()
            .optional()
            .default("months")
            .valid("months")
            .valid("years"),
          costPerMonth: joi.alternatives(joi.string(), joi.number()),
          accountNumber: joi.string().optional().default(null),
          accountIssuer: joi.string().optional().default(null),
          paymentType: joi
            .string()
            .default("CASH")
            .required()
            .valid("MOMO")
            .valid("CASH")
            .valid("BANK DEPOSIT"),
          amount: joi.alternatives(joi.string(), joi.number()).required(),
          otp: joi.string().optional(),
        }),
      },
    },
    handler: Handler.newRent,
  },
  {
    path: "/admin/new",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          room: joi.string().required(),
          tenant: joi.string().required(),
          duration: joi.alternatives(joi.string(), joi.number()),
          durationType: joi
            .string()
            .optional()
            .default("months")
            .valid("months")
            .valid("years"),
          costPerMonth: joi.alternatives(joi.string(), joi.number()),
          paymentType: joi
            .string()
            .default("CASH")
            .required()
            .valid("MOMO")
            .valid("CASH")
            .valid("BANK DEPOSIT"),
          amount: joi.alternatives(joi.string(), joi.number()).required(),
          rentType: joi.string().required().valid("NEW").valid("RENEWAL"),
        }),
      },
    },
    handler: Handler.adminAddRent,
  },
  {
    path: "/update",
    method: "PUT",
    options: {
      validate: {
        payload: joi.object({
          id: joi.string().required(),
          data: joi.object(),
        }),
      },
    },
    handler: Handler.update,
  },
  {
    path: "/get",
    method: "GET",
    handler: Handler.getall,
  },
  {
    path: "/filter",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          room: joi.string().optional().allow(null),
          accomodation: joi.string().optional().allow(null),
          status: joi.string().optional().allow(null),
          startDate: joi.string().optional().allow(null),
          endDate: joi.string().optional().allow(null),
        }),
      },
    },
    handler: Handler.filter,
  },
  {
    path: "/get/{id}",
    method: "GET",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.getRentById,
  },
  {
    path: "/pending/get",
    method: "GET",
    handler: Handler.getPending,
  },
  {
    path: "/tenant/pending/{id}",
    method: "GET",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.getTenantPendingRents,
  },
  {
    path: "/tenant/get/{id}",
    method: "GET",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.getTenantRents,
  },
  {
    path: "/approve",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          rentId: joi.string().required(),
          tenantId: joi.string().required(),
          startDate: joi.string().required(),
          endDate: joi.string().required(),
        }),
      },
    },
    handler: Handler.approveRent,
  },
  {
    path: "/details/{id}",
    method: "GET",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.rentDetails,
  },
  {
    path: "/renew",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          rent: joi.string().required(),
          duration: joi.alternatives(joi.string(), joi.number()),
          paymentType: joi
            .string()
            .default("CASH")
            .required()
            .valid("MOMO")
            .valid("CASH")
            .valid("BANK DEPOSIT"),
          accountNumber: joi.string().optional().default(null),
          accountIssuer: joi.string().optional().default(null),
          startDate: joi.string().required(),
          amount: joi.alternatives(joi.string(), joi.number()).required(),
          otp: joi.string().optional(),
        }),
      },
    },
    handler: Handler.renewRent,
  },
  {
    path: "/pay",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          rent: joi.string().required(),
          duration: joi.alternatives(joi.string(), joi.number()),
          accountNumber: joi.string().optional().default(null),
          accountIssuer: joi.string().optional().default(null),
          amount: joi.alternatives(joi.string(), joi.number()).required(),
          otp: joi.string().optional(),
          paymentType: joi
            .string()
            .default("CASH")
            .required()
            .valid("MOMO")
            .valid("CASH")
            .valid("BANK DEPOSIT"),
        }),
      },
    },
    handler: Handler.pay,
  },
  {
    path: "/security/deposit",
    method: "POST",
    options: {
      validate: {
        payload: joi.object({
          propertyId: joi.string().required(),
          accountNumber: joi.string().optional().default(null),
          accountIssuer: joi.string().optional().default(null),
          amount: joi.alternatives(joi.string(), joi.number()).required(),
          otp: joi.string().optional(),
          paymentType: joi
            .string()
            .default("CASH")
            .required()
            .valid("MOMO")
            .valid("CASH")
            .valid("BANK DEPOSIT"),
        }),
      },
    },
    handler: Handler.pay,
  },
  {
    path: "/revoke",
    method: "PUT",
    options: {
      validate: {
        payload: joi.object({
          tenantId: joi.string().required(),
          rentId: joi.string().required(),
          reason: joi.string().required(),
        }),
      },
    },
    handler: Handler.revoke,
  },
  {
    path: "/delete/{id}",
    method: "DELETE",
    options: {
      validate: {
        params: joi.object({
          id: joi.string().required(),
        }),
      },
    },
    handler: Handler.deleteRent,
  },
];

export default routes;
