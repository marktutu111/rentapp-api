import { formatePhonenumber } from "./formate-phone-number";

const utils={
    pkg:{
        name:'utils',
        version:'1'
    },
    register:async(server)=>{
        server.method('formatePhonenumber',formatePhonenumber);
    }
}


export default utils;