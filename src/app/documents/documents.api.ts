import db from "./db/documents.db";

const api={
    pkg:{
        name:'documents',
        version:'1'
    },
    register:async(server)=>{
        server.method('dbDocuments',db);
    }
}

export default api;