import * as mongoose from "mongoose";

const schema=new mongoose.Schema(
    {
        documentType:{
            type:String,
            default:null
        },
        documentId:{
            type:String,
            default:null
        },
        documentPath:{
            type:String,
            default:null
        },
        downloadUrl:{
            type:String,
            default:null
        },
        createdAt:{
            type:Date,
            default:Date.now
        },
        updatedAt:{
            type:Date,
            default:Date.now
        }
    }
);

export default mongoose.model(
    'documents',
    schema
)