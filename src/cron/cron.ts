var CronJob = require("cron").CronJob;
import { Server } from "@hapi/hapi";
import Pending from "./pending-payments";
import Reminders from "./reminders";

const cron = {
  pkg: {
    name: "cron",
    version: "1",
  },
  register: async (server: Server) => {
    try {
      const job = new CronJob("*/5 * * * * *", () => {
        console.log("checking transactions");
        Pending(server);
      });
      const reminder = new CronJob("0 0 * * *", () => {
        Reminders(server).catch((err) => null);
      });
      job.start();
      reminder.start();
    } catch (err) {
      console.log(err);
    }
  },
};

export default cron;
