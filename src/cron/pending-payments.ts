import { EnumOperators, EnumPaymentStatus } from "../app/common/payments.model";
import lazyPay from "../lazypay/lazypay.request";
import callbackHandler from "../app/payments/handler/callback.handler";
import * as moment from "moment";

const pending = async (server: any): Promise<void> => {
  try {
    const start = moment().startOf("day").toDate();
    const end = moment().endOf("day").toDate();
    const transactions = await server.methods.dbPayments.find({
      $or: [
        { status: EnumPaymentStatus.PENDING },
        { status: EnumPaymentStatus.INITIATED },
      ],
      createdAt: {
        $gte: start,
        $lte: end,
      },
    });
    for (const ref of transactions) {
      try {
        let token: any;
        const { transactionRef, operator, transaction }: any = ref;
        let response: any = null;
        switch (operator) {
          case EnumOperators.REDDE:
            response = await server.methods.WigalSendPayment({
              type: "STATUS",
              payload: transaction["transactionid"],
            });
            break;
          case EnumOperators.LAZYPAY:
            token = await lazyPay.getToken();
            response = await lazyPay.getStatus({
              id: transactionRef,
              token: token.data,
            });
            break;
          case EnumOperators.PPAY:
            token = await server.methods.getToken();
            response = await server.methods.getStatus(
              transactionRef,
              token.data
            );
            break;
          default:
            throw Error("Operator type not permitted");
        }
        callbackHandler(response);
      } catch (err) {
        console.log(err);
      }
    }
  } catch (err) {}
};

export default pending;
