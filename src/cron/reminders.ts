import * as moment from "moment";
import { formatePhonenumber } from "../app/utils/formate-phone-number";

const reminder = async (server) => {
  try {
    const { receive_sms_alerts } = await server.methods.dbSettings.findOne({});
    const rents = await server.methods.dbRents
      .find({
        status: "ACTIVE",
      })
      .populate("tenant")
      .populate("room");
    rents.forEach(async (rent) => {
      try {
        const { endDate, tenant, room, _id } = rent;
        const _sDate = moment(new Date()).startOf("day");
        const _eDate = moment(new Date(endDate)).endOf("day");
        const _diff = _eDate.diff(_sDate, "days");
        if (_diff <= 3) {
          // send notification
          const fullname = `${tenant["firstname"]} ${tenant["lastname"]}`;
          server.methods
            .sendsmsArkesel({
              message:
                "Hello " +
                fullname +
                "\n" +
                "We at 16TH AUGUST 85 VILLA  kindly reminds you of your tenancy agreement which expires on " +
                new Date(rent.endDate).toDateString() +
                ". " +
                "3 months or 6months renewal is ACCEPTED or better still you can rent for more years at " +
                room["currency"] +
                room["price"] +
                " per month. " +
                "Thanks for renting with us at " +
                "16TH AUGUST 85 VILLA.",
              destination: tenant["phone"],
            })
            .catch((err) => console.log(err));
          if (Array.isArray(receive_sms_alerts)) {
            receive_sms_alerts.forEach((cont: string): void => {
              server.methods
                .sendsmsArkesel({
                  message:
                    "RENT REMINDER\n" +
                    fullname +
                    ", Rent expires on" +
                    new Date(rent.endDate).toDateString() +
                    ". " +
                    "16TH AUGUST 85 VILLA",
                  destination: formatePhonenumber(cont),
                })
                .catch((err) => console.log(err));
            });
          }
        }
        if (_diff <= 0) {
          // Expire Rent
          await Promise.all([
            server.methods.dbRents.findOneAndUpdate(
              {
                _id: _id,
              },
              {
                $set: {
                  updatedAt: Date.now(),
                  status: "EXPIRED",
                },
              }
            ),
            server.methods.dbRooms.findOneAndUpdate(
              {
                _id: room._id,
              },
              {
                $set: {
                  occupied: false,
                },
              }
            ),
          ]);
        }
      } catch (err) {}
    });
  } catch (err) {}
};

export default reminder;
