"use strict";

import * as Hapi from "@hapi/hapi";
import * as Good from "good";
import * as mongoose from "mongoose";

import { goodConfig } from "./config/good.config";
import { _server } from "./config/server.config";
import * as path from "path";

// ROUTES //
import { Db_config } from "./config/database.config";

import OTP from "./otp/otp.api";
import Uploader from "./uploader/uploader.api";
import APP from "./app/app";
import fileManager from "./files-manager/files.api";
import Wigal from "./wigal";
import PeoplesPay from "./peoplespay/peoplespay.api";
import Utils from "./app/utils/utils.module";
import RouteMobile from "./route-mobile";
import CRON from "./cron/cron";
import Arkesel from "./arkesel/arkesel.api";

// Start the server
async function start() {
  try {
    // SERVER CONFIGURATION //
    const server: any = Hapi.server({
      port: _server.port,
      host: _server.host,
      routes: {
        cors: {
          origin: ["*"],
        },
        files: {
          relativeTo: path.resolve("uploads"),
        },
      },
    });

    await server.register(require("inert"));

    // REGISTER SERVER PLUGINS //
    await server.register([
      {
        plugin: Good,
        options: goodConfig,
      },
      Utils,
      Arkesel,
      RouteMobile,
      Uploader,
      PeoplesPay,
      {
        plugin: Wigal,
        routes: {
          prefix: "/rentcontrol/redde",
        },
      },
      CRON,
      {
        plugin: OTP,
        routes: {
          prefix: "/rentcontrol/otp",
        },
      },
      {
        plugin: fileManager,
        routes: {
          prefix: "/rentcontrol/doc",
        },
      },
    ]);

    // REGISTER SERVER ROUTES HERE //
    await server.register(APP);

    // CONNECT TO DATABASE
    await mongoose.connect(Db_config.URL);

    // START SERVER //
    await server.start();
    console.log("Server running at:", server.info.uri);
  } catch (err) {
    console.log(err);
    process.exit(1);
  }
}

start();
