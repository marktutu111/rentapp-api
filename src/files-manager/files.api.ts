const api = {
  pkg: {
    name: "file-reader",
    version: "1",
  },
  register: async (server) => {
    server.route([
      {
        path: "/{path*}",
        method: "GET",
        handler: function (request, h) {
          console.log("hello");
          return h.file(request.params.path);
        },
      },
    ]);
  },
};

export default api;
