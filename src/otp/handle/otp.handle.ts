import * as randomize from "randomatic";
import * as moment from "moment";
import { OTP } from "../db/otp.db";

const sendOtp = async (request, h) => {
  try {
    const { phoneNumber } = request.payload;
    const otp = randomize("0", 5);
    const response = await request.server.methods.dbOtp.findOneAndUpdate(
      {
        phoneNumber: phoneNumber,
      },
      {
        $set: {
          otp: otp,
          updatedAt: Date.now(),
          expires: moment().add(30, "minutes"),
        },
      },
      {
        upsert: true,
        new: true,
      }
    );

    const _formattedNumber =
      request.server.methods.formatePhonenumber(phoneNumber);
    request.server.methods
      .sendsmsArkesel({
        destination: _formattedNumber,
        message: "your OTP is " + otp + ".",
      })
      .catch((err) => null);
    return h.response({
      success: true,
      message: "success",
      data: response,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

const validateOTP = async ({ phoneNumber, otp }) => {
  try {
    const response: any = await OTP.findOne({
      phoneNumber: phoneNumber,
      otp: otp,
    });
    if (!response || !response._id) {
      throw Error("OTP Invalid");
    }
    return moment().isBefore(response.expires);
  } catch (err) {
    return false;
  }
};

const getOtp = async (request, h) => {
  try {
    const results = await request.server.methods.dbOtp.find({});
    return h.response({
      success: true,
      message: "success",
      data: results,
    });
  } catch (err) {
    return h.response({
      success: false,
      message: err.message,
    });
  }
};

export default {
  sendOtp,
  getOtp,
  validateOTP,
};
