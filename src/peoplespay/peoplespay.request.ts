import *as request from "request";
import { ResponseInterface } from "./peoplespay.model";
import { CONFIG } from "./peoplespay.config";

const  BASEURL:string='https://peoplepay.com.gh/peoplepay';

const getStatus=(id:string,token:string):Promise<ResponseInterface>=>new Promise((resolve,reject)=>{
    const url:string=`${BASEURL}/hub/transactions/get/${id}`;
    const headers={
        'Content-type':'Application/json',
        'Authorization':'Bearer '+token,
    }
    request.get(url,{headers},(err,res,bd)=>{
        try {
            if(err)throw Error(err);
            resolve(
                JSON.parse(bd)
            )
        } catch (err) {
            reject(
                err
            )
        }
    })
})

const getToken=():Promise<ResponseInterface>=>new Promise((resolve,reject)=>{
    const url:string=`${BASEURL}/hub/token/get`;
    const body=JSON.stringify(
        {
            "merchantId":CONFIG.merchantId,
            "apikey":CONFIG.apiKey
        }
    )
    const headers={
        'Content-type':'Application/json'
    }
    request.post(url,{headers,body},(err,res,bd)=>{
        try {
            if(err)throw Error(err);
            resolve(
                JSON.parse(bd)
            )
        } catch (err) {
            reject(
                err
            )
        }
    })
})



const debitMomo=({data,token}):Promise<ResponseInterface>=>new Promise((resolve,reject)=>{
    const url:string=`${BASEURL}/hub/collectmoney`;
    const headers={
        'Content-Type':'application/json',
        'Authorization':`Bearer ${token}`
    }
    request.post(url,{body:JSON.stringify(data),headers},(err,res,bd)=>{
        try {
            if(err)throw Error(err);
            resolve(
                JSON.parse(bd)
            )
        } catch (err) {
            reject(
                err
            )
        }
    })
})


export default {
    debitMomo,
    getToken,
    getStatus
}