import { Server } from "hapi";
import handler from "./peoplespay.request";

const api={
    name:'peoplespay',
    version:'1',
    register:async(server:Server)=>{
        server.method('getToken',handler.getToken);
        server.method('getStatus',handler.getStatus);
        server.method('sendPayment',handler.debitMomo);
    }
}

export default api;