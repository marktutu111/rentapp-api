
export interface ResponseInterface {
    success:boolean;
    message:string;
    data:any;
    code:string;
    transactionId?:string;
    externalTransactionId?:string;
}

export interface CollectMoneyMomoInterface {
    amount:string;
    account_name:string;
    account_number:string;
    account_issuer:string;
    description:string;
    callbackUrl:string;
}