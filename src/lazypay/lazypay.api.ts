import { Server } from "@hapi/hapi";
import handler from "./lazypay.request";

const api = {
  name: "lazypay",
  version: "1",
  register: async (server: Server) => {
    server.method("getToken", handler.getToken);
    server.method("getStatus", handler.getStatus);
  },
};

export default api;
