import * as request from "request";
import { ResponseInterface } from "./lazypay.model";
import { CONFIG } from "./lazypay.config";

const BASEURL: string = "https://lazypaygh.com/api";

const getStatus = ({ id, token }): Promise<ResponseInterface> =>
  new Promise((resolve, reject) => {
    const url: string = `${BASEURL}/hub/status/${id}`;
    const headers = {
      "Content-type": "Application/json",
      Authorization: "Bearer " + token,
    };
    request.get(url, { headers }, (err, res, bd) => {
      try {
        if (err) throw Error(err);
        resolve(JSON.parse(bd));
      } catch (err) {
        reject(err);
      }
    });
  });

const getToken = (
  operation: "DEBIT" | "CREDIT" = "DEBIT"
): Promise<ResponseInterface> =>
  new Promise((resolve, reject) => {
    const url: string = `${BASEURL}/hub/token`;
    const body = JSON.stringify({
      merchantId: CONFIG.merchantId,
      apikey: CONFIG.apiKey,
      operation,
    });
    const headers = {
      "Content-type": "Application/json",
    };
    request.post(url, { headers, body }, (err, res, bd) => {
      try {
        if (err) throw Error(err);
        resolve(JSON.parse(bd));
      } catch (err) {
        reject(err);
      }
    });
  });

const debitCard = ({ data, token }): Promise<ResponseInterface> =>
  new Promise((resolve, reject) => {
    const body = JSON.stringify({
      ...data,
      callbackUrl: CONFIG.callbackUrl
    });
    const url: string = `${BASEURL}/hub/card`;
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    request.post(url, { body, headers }, (err, res, bd) => {
      try {
        const response = JSON.parse(bd);
        if (err || !response.success) throw Error(err || response.message);
        resolve(response);
      } catch (err) {
        reject(err);
      }
    });
  });

const debitMomo = ({ data, token }): Promise<ResponseInterface> =>
  new Promise((resolve, reject) => {
    const amount = Number(data.amount).toFixed(2);
    const account_issuer = data.account_issuer.toLowerCase();
    const body = JSON.stringify({
      ...data,
      amount,
      account_issuer,
      callbackUrl: CONFIG.callbackUrl,
    });
    const url: string = `${BASEURL}/hub/debit`;
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    request.post(url, { body, headers }, (err, res, bd) => {
      try {
        const response = JSON.parse(bd);
        if (err || !response.success) throw Error(err || response.message);
        resolve(response);
      } catch (err) {
        reject(err);
      }
    });
  });

const cashout = ({ data, token }): Promise<ResponseInterface> =>
  new Promise((resolve, reject) => {
    const amount = Number(data.amount).toFixed(2);
    const account_issuer = data.account_issuer.toLowerCase();
    const body = JSON.stringify({
      ...data,
      amount,
      account_issuer,
      callbackUrl: CONFIG.callbackUrl,
    });
    const url: string = `${BASEURL}/hub/credit`;
    const headers = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    request.post(url, { body, headers }, (err, res, bd) => {
      try {
        if (err) throw Error(err);
        resolve(JSON.parse(bd));
      } catch (err) {
        reject(err);
      }
    });
  });

export default {
  debitMomo,
  getToken,
  getStatus,
  cashout,
  debitCard,
};
