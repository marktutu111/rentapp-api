import * as request from "request";
import { formatePhonenumber } from "../../app/utils/formate-phone-number";
import { config } from "../config";

const SendSms = ({ message, destination, source = "16TH AUGUST 85 VILLA" }) => {
  const { username, password } = config;
  const url: string =
    "http://api.rmlconnect.net:8080/bulksms/bulksms?" +
    "username=" +
    username +
    "&password=" +
    password +
    "&type=0" +
    "&dlr=0" +
    "&destination=" +
    formatePhonenumber(destination) +
    "&source=16thVilla" +
    "&message=" +
    message +
    "&url=";
  const options = {
    url: url,
    headers: {
      "Content-type": "Application/json",
    },
  };
  return new Promise((resolve, reject) => {
    request.get(options, (err, res, bd) => {
      try {
        console.log(bd);
        if (err) reject(err);
        resolve(bd);
      } catch (err) {
        reject(err);
      }
    });
  });
};

export default SendSms;
