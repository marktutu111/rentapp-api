import { Server } from "hapi";
import dbSettings from "../app/settings/db/settings.db";
import SendSms from "./process/send-request";

const routeMobile = {
  pkg: {
    name: "route-mobile",
    version: "1",
  },
  register: async (server: Server) => {
    const sendRequest = async (data: any): Promise<any> => {
      try {
        const settings = await dbSettings.Settings.findOneAndUpdate(
          {},
          {
            $set: {
              updatedAt: Date.now(),
            },
            $inc: {
              smsBalance: -1,
            },
          },
          { new: true }
        );
        if (!settings && !settings._id) {
          throw Error(
            "You dont have enough balance to process your transaction"
          );
        }
        await dbSettings.Sms.create({
          message: data["message"],
        });
        return SendSms(data);
      } catch (err) {
        return err;
      }
    };

    server.method("sendSMS", sendRequest);
  },
};

export default routeMobile;
